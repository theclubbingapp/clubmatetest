/** @format */

// import {AppRegistry} from 'react-native';
// import App from './src/App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);
import React from 'react'
import { AppRegistry } from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import bgMessaging from './src/components/common/bgMessaging';
// redux
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './src/reducers'
import thunk from 'redux-thunk'
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');
const store = createStore(rootReducer, applyMiddleware(thunk))
// App
const GoToTravel = () => (
    <Provider store={store}>
      <App />
    </Provider>
  )
  
  AppRegistry.registerComponent(appName, () => GoToTravel);
  AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);