import {
    REGISTER_SUCCESS, REGISTER_FAIL, REGISTER_USER, OTP_SUCCESS, OTP_USER, OTP_FAIL, OTP_VERIFY_SUCCESS,
    OTP_VERIFY_FAIL, OTP_VERIFY_USER
} from './actionTypes';
import APIURLCONSTANTS from "../ApiUrlList";
import axios from 'axios';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
import ApiUtils from '../ApiUtils';






export const registerUser = () => {

    return {
        type: REGISTER_USER,
        payload: true
    }
};

export const registerSuccess = (response) => {

    return {
        type: REGISTER_SUCCESS,
        payload: response
    }
};

export const registerFail = (error) => {
    // alert(error)

    return {
        type: REGISTER_FAIL,
        payload: error
    }
};

export function registerCall(devicetoken, username, password, imei, ostype, phone, gender, dob, food, dance, bio, typeofdrink, interestedin) {
    return function (dispatch) {
        dispatch(registerUser())


        const formData = new FormData()
        formData.append('phone_number', phone)
        formData.append('full_name', username)
        formData.append('dob', dob)
        formData.append('password', password)
        formData.append('gender', JSON.stringify(gender))
       
        formData.append('food', JSON.stringify(food))
        formData.append('dance', JSON.stringify(dance))
        formData.append('type_of_drink', JSON.stringify(typeofdrink))
        formData.append('intrested_in', JSON.stringify(interestedin))
        formData.append('bio', bio)
        formData.append('devicetoken', devicetoken)
        formData.append('imei', imei)
        formData.append('ostype', JSON.stringify(ostype))
        console.log('register form'+JSON.stringify(formData));
        axios.post(APIURLCONSTANTS.REGISTER_USER_URL, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => dispatch(registerSuccess(res.data)))
            .catch(error => {
                // alert(JSON.stringify(error))
                if (error.response) {
                    // alert(JSON.stringify(error))
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // console.log(error.response.data);
                    // console.log(error.response.status);
                    // console.log(error.response.headers);
                    // alert(JSON.stringify(error.response)),
                    dispatch(registerFail(error.response.data.msg))
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    // console.log(error.request);
                    // dispatch(registerFail(error.request))
                    dispatch(loginFail(error.request._response))

                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    dispatch(registerFail(error.message))

                }
                console.log(error.config);
                // dispatch(logInFailure(error))
            });

    }
};

