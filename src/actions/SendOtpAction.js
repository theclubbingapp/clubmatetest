import {
   OTP_SUCCESS, OTP_USER, OTP_FAIL
} from './actionTypes';
import APIURLCONSTANTS from "../ApiUrlList";
import axios from 'axios';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
import ApiUtils from '../ApiUtils';

export const sendOtps = () => {
    console.log('sendOtps');
    return {
        type: OTP_USER,
        payload: true
    }
};

export const sendOtpSuccess = (r) => {
    console.log('sendOtps sendOtpSuccess' + r);

    return {
        type: OTP_SUCCESS,
        payload: r
    }
};

export const sendOtpFail = (error) => {
    // alert(error)
    console.log('sendOtps error' + error);
    return {
        type: OTP_FAIL,
        payload: error
    }
};





export function sendOtp(phone) {
    return function (dispatch) {

        dispatch(sendOtps())
        let body = JSON.stringify({ phone })
        console.log("body" + body);
        axios.post(APIURLCONSTANTS.SEND_OTP, body, null)
            .then(ApiUtils.checkStatus)
            .then(res => dispatch(sendOtpSuccess(res.data.message)))
            .catch(error => {
                // alert(JSON.stringify(error))
                if (error.response) {
                    // alert(JSON.stringify(error))
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // console.log(error.response.data);
                    // console.log(error.response.status);
                    // console.log(error.response.headers);
                    // alert(JSON.stringify(error.response)),
                    dispatch(sendOtpFail(error.response.data.message))
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    // console.log(error.request);
                    dispatch(sendOtpFail(error.request))

                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    dispatch(sendOtpFail(error.message))

                }
                console.log(error.config);
                // dispatch(logInFailure(error))
            });

    }



}

