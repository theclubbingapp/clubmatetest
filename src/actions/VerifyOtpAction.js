import {
    OTP_VERIFY_SUCCESS,
    OTP_VERIFY_FAIL, OTP_VERIFY_USER
} from './actionTypes';
import APIURLCONSTANTS from "../ApiUrlList";
import axios from 'axios';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
import ApiUtils from '../ApiUtils';



export const verifyOtps = () => {
    console.log('verfy otp verifyOtps');
    return {
        type: OTP_VERIFY_USER,
        payload: true
    }
};

export const verifyOtpSuccess = (response) => {
    console.log('verfy otp verifyOtpSuccess' + response);
    // alert('suvvess')
    return {
        type: OTP_VERIFY_SUCCESS,
        payload: response
    }
};

export const verifyOtpFail = (error) => {
    console.log('verfy otp verifyOtpFail');

    return {
        type: OTP_VERIFY_FAIL,
        payload: error
    }
};


export function verifyOtp(authtoken,otp) {
    return function (dispatch) {
        dispatch(verifyOtps())
        const formData = new FormData()
        formData.append('authtoken', authtoken)
        formData.append('code', otp)
        console.log('verifyotp' + JSON.stringify(formData));
        axios.post(APIURLCONSTANTS.VERIFY_OTP, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => dispatch(verifyOtpSuccess(res.data.msg)))
            .catch(error => {
                // alert(JSON.stringify(error))
                if (error.response) {
                    // alert(JSON.stringify(error))
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // console.log(error.response.data);
                    // console.log(error.response.status);
                    // console.log(error.response.headers);
                    // alert(JSON.stringify(error.response)),
                    dispatch(verifyOtpFail(error.response.data.msg))
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    // console.log(error.request);
                    dispatch(verifyOtpFail(error.request))

                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    dispatch(verifyOtpFail(error.message))

                }
                console.log(error.config);
                // dispatch(logInFailure(error))
            });

    }
}
