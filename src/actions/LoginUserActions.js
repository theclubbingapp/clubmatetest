import {
  LOGIN_USER, LOGIN_SUCCESS, LOGIN_FAIL,
  FAKE_LOGIN, LOGOUT
} from './actionTypes';
import APIURLCONSTANTS from "../ApiUrlList";
import axios from 'axios';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
import ApiUtils from '../ApiUtils';
export const logoutUser = () => {
  console.log('logoutcalledddd')
  return {
    type: LOGOUT,
    payload: true
  }
};

export const loginUser = () => {

  return {
    type: LOGIN_USER,
    payload: true
  }
};

export const loginSuccess = (response) => {
  console.log('login success' + JSON.stringify(response))

  return {
    type: LOGIN_SUCCESS,
    payload: response
  }
};

export const loginFail = (error) => {
  // alert(error)

  return {
    type: LOGIN_FAIL,
    payload: error
  }
};

export function loginCall(phone, password, devicetoken, ostype,imei ) {
  return function (dispatch) {
    dispatch(loginUser())

    if (phone.length == 0) {
      dispatch(loginFail("Please enter number"))
      return;
    }

    if (password.length == 0) {
      dispatch(loginFail('Please enter password'))
      return;
    }

    const formData = new FormData()
    formData.append('phone_number', phone)
    formData.append('password', password)
    formData.append('devicetoken', devicetoken)
    formData.append('imei', imei)
    formData.append('ostype', JSON.stringify(ostype))
    // console.log(JSON.stringify(formData));
    axios.post(APIURLCONSTANTS.LOGIN, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => dispatch(loginSuccess(res.data)))
      .catch(error => {
        console.log('login fail'+JSON.stringify(error))
        if (error.response) {
          dispatch(loginFail(error.response.data.msg))
        } else if (error.request) {
          dispatch(loginFail(error.request._response))
        } else {
          // console.log('Error', error.message);
          dispatch(loginFail(error.message))
        }
        console.log(error.config);
      });

  }
};


