  export const FAKE_LOGIN='FAKE_LOGIN';
  export const LOGOUT='LOGOUT';
  export const LOGIN_SUCCESS='LOGIN_SUCCESS';
  export const LOGIN_FAIL='LOGIN_FAIL';
  export const LOGIN_USER='LOGIN_USER';

  export const REGISTER_SUCCESS='REGISTER_SUCCESS';
  export const REGISTER_FAIL='REGISTER_FAIL';
  export const REGISTER_USER='REGISTER_USER';

  export const OTP_SUCCESS='OTP_SUCCESS';
  export const OTP_FAIL='OTP_FAIL';
  export const OTP_USER='OTP_USER';

  export const OTP_VERIFY_SUCCESS='OTP_VERIFY_SUCCESS';
  export const OTP_VERIFY_FAIL='OTP_VERIFY_FAIL';
  export const OTP_VERIFY_USER='OTP_VERIFY_USER';

  export const GET_PACKAGES_SUCCESS='GET_PACKAGES_SUCCESS';
  export const GET_PACKAGES_FAIL='GET_PACKAGES_FAIL';
  export const GET_PACKAGES='GET_PACKAGES';

  export const RELOAD='RELOAD';


  




















