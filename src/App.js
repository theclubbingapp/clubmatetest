/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// mkdir android/app/src/main/assets

// react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, AsyncStorage, View, Alert } from 'react-native';
import { Provider } from "react-redux";
import reducers from './reducers';

import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import Router from './Router'
import HomeRouter from './HomeRouter'
import keys from './preference'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as AppActions from "../src/actions/index";
import firebase from 'react-native-firebase';
import { GlobalClass } from './GlobalClass.js';
// import type { Notification, NotificationOpen } from 'react-native-firebase';
import firebaseService from '../src/service/firebase'
// const FIREBASE_REF_MESSAGES



class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      fromNotification: false,
      isLoading: false,
      gotoFriendScreen: false,
    };

  }

  //  static allFunction = () => {
  //   this.setState({
  //     isLoggedIn: false
  //   })
  // }

  componentWillUnmount() {
    global.hit = false;
    // this.notificationListener();
    // this.notificationOpenedListener();
    this.notificationDisplayedListener();
    this.notificationListener();
    this.onTokenRefreshListener();
    // this.notificationOpenedListener();

  }



  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  _storeData = async (fcmToken) => {
    try {
      await AsyncStorage.setItem(keys.fcmToken, fcmToken);
    } catch (error) {
      // Error saving data
    }
  };

  async componentDidMount() {
    this.checkPermission();
    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      if (fcmToken) {
        console.log('fcmtoke' + fcmToken);
        this._storeData(fcmToken)
        // user has a device token
        // await AsyncStorage.setItem(keys.fcmToken, fcmToken);
      }
    });
    // global.isChatOpen=false;
    // global.chatToken=''
    const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const action = notificationOpen.action;
      const notification: Notification = notificationOpen.notification;
      var seen = [];
      console.log('datasa notifivation' + JSON.stringify(notification.data))
      this.resolveData(notification.data)

    }
    // const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
    // .setDescription('My apps test channel');
    // Create the channel
    // firebase.notifications().android.createChannel(channel);

    const ChatchannelId = new firebase.notifications.Android.Channel('chat-channel', 'chat-channel', firebase.notifications.Android.Importance.Max)
      .setSound('default').setShowBadge(true)
      .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
      .enableLights(true)
      .enableVibration(true).setVibrationPattern([3000, 300, 500, 300]).setDescription('Chat Notification');;
    // Create the channel
    firebase.notifications().android.createChannel(ChatchannelId);

    const BroadcastchannelId = new firebase.notifications.Android.Channel('broadcast-channel', 'broadcast-channel', firebase.notifications.Android.Importance.Max)
      .setSound('default')
      .setShowBadge(true)
      .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
      .enableLights(true)
      .enableVibration(true).setVibrationPattern([0, 500, 0, 2000, 0, 500]).setDescription('Broadcast Notification');;
    // Create the channel
    firebase.notifications().android.createChannel(BroadcastchannelId);

    const RequestchannelId = new firebase.notifications.Android.Channel('request-channel', 'request-channel', firebase.notifications.Android.Importance.Max)
      .setSound('default').setShowBadge(true)
      .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
      .enableLights(true)
      .enableVibration(true).setVibrationPattern([3000, 300, 500, 300]).setDescription('Request Notification');;
    // Create the channel
    firebase.notifications().android.createChannel(RequestchannelId);


    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    });
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
      console.log('notification' + JSON.stringify(notification.data))
      console.log('isChatOpen' + JSON.stringify(global.isChatOpen))
      console.log('chatToken' + JSON.stringify(global.chatToken))
      console.log('myToken' + JSON.stringify(notification.data['myToken']))

      // if (Platform.OS == 'ios') {
      //   if (notification.tag == 'chat') {
      //     if (global.isChatOpen) {
      //       if (global.chatToken == notification.badge) {
      //       } else {
      //         this.showNotificationIos(notification)
      //       }
      //     } else {
      //       this.showNotificationIos(notification)
      //     }
      //   } else {
      //     this.showNotificationIos(notification)
      //   }
      // }
      // else {
      if (notification.data['type'] == 'chat') {
        if (global.isChatOpen) {
          if (global.chatToken == notification.data['myToken']) {

          } else {
            // ChatchannelId.setGroup(notification.data['myToken']);
            this.showChatNotification(notification, ChatchannelId, 'chat-channel')
          }

        } else {
          // ChatchannelId.setGroup(notification.data['myToken']);
          this.showChatNotification(notification, ChatchannelId, 'chat-channel')
        }
      } else if (notification.data['type'] == 'broadcast') {
        this.showBroadcastNotification(notification, BroadcastchannelId, 'broadcast-channel')
      }
      else {
        this.showNotification(notification, RequestchannelId, 'request-channel')
      }
      // }

      // if (notification.data['type'] == 'chat') {
      //   if (global.isChatOpen) {
      //     if (global.chatToken == notification.data['myToken']) {

      //     } else {
      //       // ChatchannelId.setGroup(notification.data['myToken']);
      //       this.showChatNotification(notification, ChatchannelId, 'chat-channel')
      //     }

      //   } else {
      //     // ChatchannelId.setGroup(notification.data['myToken']);
      //     this.showChatNotification(notification, ChatchannelId, 'chat-channel')
      //   }
      // } else if (notification.data['type'] == 'broadcast') {
      //   this.showBroadcastNotification(notification, BroadcastchannelId, 'broadcast-channel')
      // }
      // else {
      //   this.showNotification(notification, RequestchannelId, 'request-channel')
      // }

    });
    // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
    //   // Get the action triggered by the notification being opened
    //   const action = notificationOpen.action;
    //   // Get information about the notification that was opened
    //   const notification: Notification = notificationOpen.notification;
    //   console.log('d notifivation' + JSON.stringify(notification.data))
    //   this.resolveData(notification.data)



    //   // var seen = [];
    //   // alert(JSON.stringify(notification.data, function (key, val) {
    //   //   if (val != null && typeof val == "object") {
    //   //     if (seen.indexOf(val) >= 0) {
    //   //       return;
    //   //     }
    //   //     seen.push(val);
    //   //   }
    //   //   return val;
    //   // }));
    // firebase.notifications().removeDeliveredNotification(notification.notificationId);

    // });

    let ref = this;
    try {
      AsyncStorage.getItem(keys.isLoggedIn).then((value) => {
        console.log('isLoggedIn' + value)
        if (value == 'true') {
          ref.setState({
            isLoggedIn: true,
            isLoading: false,
          })
        } else {
          ref.setState({
            isLoggedIn: false,
            isLoading: false,
          })
        }
      });
      // this.setState({ isLoading: false })
    } catch (err) {
      ref.setState({ isLoading: false })
    }


  }
  showBroadcastNotification = (notification, channelId, name) => {

    // if (Platform.OS === 'android') {
    //   const newNotification = new firebase.notifications.Notification({
    //     show_in_foreground: true,
    //     show_in_background: true
    //   })
    //     .android.setChannelId(name)
    //     .setNotificationId(new Date().getMilliseconds().toString())
    //     .setTitle(notification.title)
    //     .setBody(notification.body)
    //     .setSound('default')
    //     .setData(notification.data)
    //     .android.setAutoCancel(true)
    //     .android.setVibrate([0, 500, 0, 2000, 0, 500])
    //     .android.setBigText(notification.body)
    //     .android.setVisibility(firebase.notifications.Android.Visibility.Public)
    //     .android.setSmallIcon('ic_launcher')
    //     .android.setCategory(firebase.notifications.Android.Category.Social)
    //   firebase.notifications().displayNotification(newNotification)

    // }
    // else if (Platform.OS === 'ios') {
    //   const localNotification = new firebase.notifications.Notification()
    //     .setNotificationId(new Date().getMilliseconds().toString())
    //     .setTitle(notification.title)
    //     .setBody(notification.body)
    //     .setData(notification.data)
    //   firebase.notifications()
    //     .displayNotification(localNotification)
    //     .catch(err => console.error(err));

    // }

    const newNotification = new firebase.notifications.Notification()
      .android.setChannelId(name)
      .setNotificationId(new Date().getMilliseconds().toString())
      .setTitle(notification.title)
      .setBody(notification.body)
      .setSound('default')
      .setData(notification.data)
      .android.setAutoCancel(true)
      .android.setVibrate([0, 500, 0, 2000, 0, 500])
      .android.setBigText(notification.body)
      .android.setVisibility(firebase.notifications.Android.Visibility.Public)
      .android.setSmallIcon('ic_launcher')
      .android.setCategory(firebase.notifications.Android.Category.Social)
      firebase.notifications().displayNotification(newNotification)


  }

  showChatNotification = (notification, channelId, name) => {
    const newNotification = new firebase.notifications.Notification()
      .android.setChannelId(name)
      .setNotificationId(new Date().getMilliseconds().toString())
      .setTitle(notification.title)
      .setBody(notification.body)
      .setSound('default')
      .setData(notification.data)
      .android.setAutoCancel(true)
      .android.setAutoCancel(true)
      .android.setBigText(notification.body)
      .android.setVibrate([3000, 300, 500, 300])
      // .android.setGroup(notification.data['myToken'])
      // .android.setGroup(notification.data['myToken'])
      .android.setVisibility(firebase.notifications.Android.Visibility.Public)
      // .android.setNumber(4)
      // .android.setBigPicture('http://clubmate.dataklouds.co.in/upload_files/users/10.jpg')
      .android.setSmallIcon('ic_launcher')
      .android.setCategory(firebase.notifications.Android.Category.Social)
    firebase.notifications().displayNotification(newNotification)
  }

  showNotificationIos = (notification) => {
    const newNotification = new firebase.notifications.Notification()
      .setNotificationId(new Date().getMilliseconds().toString())
      .setTitle(notification.title)
      .setBody(notification.body)
      .setSound('default')
      .setData(notification.data)
    firebase.notifications().displayNotification(newNotification)
  }


  showNotification = (notification, channelId, name) => {
    const newNotification = new firebase.notifications.Notification()
      .android.setChannelId(name)
      .setNotificationId(new Date().getMilliseconds().toString())
      .setTitle(notification.title)
      .setBody(notification.body)
      .setSound('default')
      .setData(notification.data)
      .android.setAutoCancel(true)
      .android.setAutoCancel(true)
      .android.setVibrate([3000, 300, 500, 300])
      // .android.setGroup(notification.data['myToken'])
      .android.setVisibility(firebase.notifications.Android.Visibility.Public)
      // .android.setNumber(4)
      // .android.setBigPicture('http://clubmate.dataklouds.co.in/upload_files/users/10.jpg')
      .android.setSmallIcon('ic_launcher')
      .android.setCategory(firebase.notifications.Android.Category.Social)

    // Build a channel
    // const channelId = new firebase.notifications.Android.Channel('chat-channel', 'chat-channel', firebase.notifications.Android.Importance.Max)
    //   .setSound('default').setShowBadge(true)
    //   .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
    //   .enableLights(true)
    //   .enableVibration(true).setVibrationPattern([3000, 300, 500, 300]).setDescription('Clubmate Channel');;
    // // Create the channel
    // firebase.notifications().android.createChannel(channelId);
    firebase.notifications().displayNotification(newNotification)
  }

  // fC44q2nfh2A:APA91bFbN4mkGVJI9SqFJmEzvdJYcYSRGo0ROnbXcTiypBOC7gdwhvvta52_22ct3cDEo0j4FokjJ3m7Ta4u7nXTFiiX9Px4stw_NiR2RwlS6qduEd4h7T17GQyA07syenPf4qLXpQUU
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem(keys.fcmToken);
    console.log('fcmtoke' + fcmToken);
    if (!fcmToken) {
      // if (Platform.OS == 'ios') {
      // fcmToken = await firebase.messaging().ios.getAPNSToken();
      // } else {
      fcmToken = await firebase.messaging().getToken();
      // }

      if (fcmToken) {
        console.log('fcmtoke' + fcmToken);
        // user has a device token
        await AsyncStorage.setItem(keys.fcmToken, fcmToken);
      }
    }
  }

  resolveData = (data) => {
    this.setState({
      fromNotification: true,
    })
    var type = '';
    if (data != null) {
      type = data['type'];
      console.log('datas notofiaction' + JSON.stringify(data.type))
      if (type == 'frienRequest') {
        global.gotoFriendScreen = true
        global.requestid = data['requestid']
        global.id = data['id']
        global.event_id = null
        global.user_authtoken = data['user_authtoken']
        global.type = 'frienRequest'
      }
      else if (type == 'pair') {
        global.gotoFriendScreen = true
        global.requestid = data['requestid']
        global.id = data['id']
        global.event_id = data['event_id']
        global.user_authtoken = data['user_authtoken']
        global.type = 'pair'
      }
      else if (type == 'chat') {
        global.type = 'chat'
        global.gotoFriendScreen = false
        global.gotoChatScreen = true
        global.phone_number = data['phone_number']
        global.myPhoneNumber = data['myPhoneNumber']
        global.myImage = data['myImage']
        global.otherUserImage = data['otherUserImage']
        global.name = data['name']
        // global.user_authtoken = data['authtoken']
        global.user_authtoken = data['myToken']
        global.device_token = data['device_token']
        global.event_id = data['event_id'],
          console.log('is running')



      }
      else if (type == 'broadcast') {
        global.type = 'broadcast'
        global.gotoFriendScreen = false
        global.gotoChatScreen = false
        //   global.phone_number = data['phone_number']
        //   global.myPhoneNumber = data['myPhoneNumber']
        //   global.myImage = data['myImage']
        //   global.otherUserImage = data['otherUserImage']
        //   global.name = data['name']
        //   global.user_authtoken = data['authtoken']
        //   global.device_token = data['device_token']
        //   global.event_id=data['event_id'],
        //  console.log('is running')



      }

    }

    this.setState({
      isLoggedIn: true
    })

  }

  //2
  async requestPermission() {
    try {
      // await firebase.messaging().requestPermission();
      firebase.messaging().requestPermission()
        .then(() => {
          if (Platform.OS == 'ios') {
            firebase.messaging().ios.registerForRemoteNotifications()
          }

        })
        .catch(error => {

        });
      this.getToken();
      // User has authorised

    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async componentWillMount() {
    // alert(JSON.stringify(this.props.auth.signInSuccess)+'this.props.auth')
    // StatusBar.setHidden(true)
    let ref = this;
    // SplashScreen.hide();
    // NetInfo.isConnected.addEventListener(
    //   'connectionChange', this._handleConnectivityChange,
    // )
    // NetInfo.isConnected.fetch().done(isConnected => {
    //   this.setState({
    //     isConnected
    //   });
    // })
    // try {
    //   AsyncStorage.getItem(keys.isLoggedIn).then((value) => {
    //     if (value == 'true') {
    //       // alert(value)
    //       ref.setState({
    //         isLoggedIn: true,
    //         isLoading: false,
    //       })
    //     } else {
    //       // alert(value)
    //       ref.setState({
    //         isLoggedIn: false,
    //         isLoading: false,
    //       })
    //     }
    //   });
    //   // this.setState({ isLoading: false })
    // } catch (err) {
    //   ref.setState({ isLoading: false })
    // }
  }





  async componentWillReceiveProps(nextProps) {
    // alert(JSON.stringify(nextProps.auth)+'nextProps.auth')
    // let ref = this;
    // if (nextProps.login.isLoggedIn != this.state.isLoggedIn) {
    //   this.setState({
    //     isLoggedIn: nextProps.login.isLoggedIn,
    //     isLoading: nextProps.login.isLoading,
    //   })
    // }

    // console.log('sign in success result'+JSON.stringify(nextProps))
    // return;


    let ref = this;
    console.log('isLoggedIn proppp' + JSON.stringify(nextProps))
    try {
      AsyncStorage.getItem(keys.isLoggedIn).then((value) => {
        if (value != 'true') {

          if (nextProps.login.isLoggedIn != this.state.isLoggedIn) {
            if (nextProps.login.isLoggedIn) {
              this.setState({
                isLoggedIn: nextProps.login.isLoggedIn
              }, () => {
                // global.myToken=nextProps.login.loginResponseData.authtoken;
                global.profileImage = '';
                if (nextProps.login.loginResponseData.image != null && nextProps.login.loginResponseData.image != '') {
                  global.profileImage = nextProps.login.loginResponseData.image;
                }
                ref.storeItem(keys.image, nextProps.login.loginResponseData.image);
                ref.storeItem(keys.userId, nextProps.login.loginResponseData.user_id);
                ref.storeItem(keys.username, nextProps.login.loginResponseData.username);
                ref.storeItem(keys.phone, nextProps.login.loginResponseData.phone_number);
                ref.storeItem(keys.token, nextProps.login.loginResponseData.authtoken);

                console.log('token' + nextProps.login.loginResponseData.authtoken)

                ref.storeItem(keys.isLoggedIn, 'true');
              })
            } else {
              console.log(nextProps.login.isLoggedIn + '==false=' + this.state.isLoggedIn);
              this.setState({
                isLoggedIn: nextProps.login.isLoggedIn
              }, () => {
                ref.storeItem(keys.isLoggedIn, 'false');
              })
            }
          }

        } else {
          console.log(nextProps.login.isLoggedIn + '==two=' + this.state.isLoggedIn);
          // alert(value)
          ref.setState({
            isLoggedIn: true,
            isLoading: false,
          })
        }
      });
      // this.setState({ isLoading: false })
    } catch (err) {
      ref.setState({ isLoading: false })
    }
    // this.setState({ user: { username: nextProps.auth.user.username } })



  }

  async storeItem(key, item) {
    let ref = this;
    try {
      //we want to wait for the Promise returned by AsyncStorage.setItem()
      //to be resolved to the actual value before returning the value
      var jsonOfItem = await AsyncStorage.setItem(key, item);
      return jsonOfItem;
    } catch (error) {
      console.log(error.message);
    }
  }


  shouldComponentUpdate(prevProps, prevState) {
    console.log(prevState.isLoggedIn + '===' + this.state.isLoggedIn);
    console.log('isLoggedIn shouldComponentUpdate' + JSON.stringify(prevState.isLoggedIn + '===' + this.state.isLoggedIn))
    if (prevState.isLoggedIn == this.state.isLoggedIn) {
      if (this.state.fromNotification) {
        this.setState({
          fromNotification: false
        })
        return true
      }

      return false;
    } else {
      return true;
    }
    // try {
    //   try {
    //     AsyncStorage.getItem(keys.isLoggedIn).then((value) => {
    //       if (value != 'true') {
    //         if (nextProps.login.isLoggedIn) {
    //           // alert(JSON.stringify(nextProps.auth))
    //           this.setState({
    //             signInSuccess: nextProps.login.isLoggedIn
    //           }, () => {
    //             ref.storeItem(keys.userId, nextProps.login.loginResponseData.data.userId);
    //             // ref.storeItem(keys.email, nextProps.login.loginResponseData.data.Email);
    //             ref.storeItem(keys.username, nextProps.login.loginResponseData.data.username);
    //             ref.storeItem(keys.phone, nextProps.login.loginResponseData.data.phone);
    //             ref.storeItem(keys.token, nextProps.login.loginResponseData.data.token);
    //             ref.storeItem(keys.isLoggedIn, 'true');
    //           })
    //         } else {
    //           this.setState({
    //             isLoggedIn: nextProps.login.loginResponseData.isLoggedIn
    //           }, () => {
    //             ref.storeItem(keys.isLoggedIn, 'false');
    //           })
    //         }
    //       } else {
    //         // alert(value)
    //         ref.setState({
    //           isLoggedIn: true,
    //           isLoading: false,
    //         })
    //       }
    //     });
    //     // this.setState({ isLoading: false })
    //   } catch (err) {
    //     ref.setState({ isLoading: false })
    //   }
    //   // this.setState({ user: { username: nextProps.auth.user.username } })
    // } catch (err) {
    //   this.setState({ isLoggedIn: false })
    // }
  }




  render() {
    // if (Platform.OS == 'ios') {
    //   StatusBar.setBarStyle('dark-content', true);
    // }
    if (this.state.isLoading) return null
    let loggedIn = false
    if (this.state.isLoggedIn) {
      loggedIn = true
    }
    // alert(loggedIn + '')
    if (!loggedIn) {
      return (
        <View style={{ flex: 1 }}>
          <Router />
          {/* {!this.state.isConnected?this.MiniOfflineSign():null} */}
          {/* <FlashMessage icon={'warning'} position="bottom" /> */}
        </View>

      )
    }
    return (

      <View style={{ flex: 1 }}>
        <HomeRouter />
        {/* {!this.state.isConnected?this.MiniOfflineSign():null} */}
        {/* <FlashMessage icon={'warning'} position="bottom" /> */}
      </View>


    )

  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(AppActions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    login: state.login
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
