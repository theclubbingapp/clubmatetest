import { combineReducers } from 'redux';
import LoginReducer from "./LoginReducer";
import RegisterReducer from "./RegisterReducer";
import PackageReducer from "./PackageReducer";
import SendOtpReducer from "./SendOtpReducer";
import VerifyOtpReducer from "./VerifyOtpReducer";
import ProfileReducer from "./ProfileReducer";


export default combineReducers({
   login: LoginReducer,
   register: RegisterReducer,
   packages: PackageReducer,
   sendotp: SendOtpReducer,
   verifyotp: VerifyOtpReducer,
   profile: ProfileReducer,
  
});