import {
    LOGIN_USER,
    LOGIN_SUCCESS, LOGIN_FAIL, FAKE_LOGIN, LOGOUT
} from "../actions/actionTypes";

const INITIAL_STATE = {
    loginResponseData: '',
    isLoading: false,
    isLoggedIn: false,
    isSuccess: false,
    
}

export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    console.log('reducer' + JSON.stringify(action.payload))
    switch (action.type) {

        case LOGIN_SUCCESS:

            return { ...state, loginResponseData: action.payload, isLoading: false, isSuccess: true, isLoggedIn: true }

        case LOGIN_FAIL:
            return { ...state, loginResponseData: action.payload, isLoading: false, isSuccess: false, isLoggedIn: false }

        case LOGIN_USER:

            return { ...state, loginResponseData: '', isLoading: true, isSuccess: false, isLoggedIn: false }


        case LOGOUT:
            console.log('logout reducer')
            return {
                ...state, loginResponseData: '',
                isLoading: false,
                isLoggedIn: false,
                isSuccess: false
            }

        default:
            return state;

    }

};