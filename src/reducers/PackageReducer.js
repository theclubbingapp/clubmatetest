import {
    GET_PACKAGES_SUCCESS, GET_PACKAGES_FAIL, GET_PACKAGES,

} from '../actions/actionTypes';
const INITIAL_STATE = {
    response: '',
    isLoading: false,
    isSuccess: false
}

export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    switch (action.type) {

        case GET_PACKAGES_SUCCESS:

            return { ...state, response: action.payload, isLoading: false, isSuccess: true }

        case GET_PACKAGES_FAIL:
            return { ...state, response: action.payload, isLoading: false, isSuccess: false }

        case GET_PACKAGES:

            return { ...state, response: '', isLoading: true, isSuccess: false }




        default:
            return state;

    }

};