import {
    RELOAD
} from "../actions/actionTypes";

const INITIAL_STATE = {
    isReload: false,
}

export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    console.log('reducer' + JSON.stringify(action.payload))
    switch (action.type) {

        case RELOAD:

            return { ...state, isReload: true }

        
        default:
            return state;

    }

};