import {
    OTP_SUCCESS, OTP_FAIL, OTP_USER
} from "../actions/actionTypes";

const INITIAL_STATE = {
    sendOtpResponse: '',
    sendOtpisSuccess: false,
    sendOtpisLoading: false,
}



export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    switch (action.type) {


        case OTP_SUCCESS:

            return { ...state, sendOtpResponse: action.payload, sendOtpisLoading: false, sendOtpisSuccess: true, }

        case OTP_FAIL:
            return { ...state, sendOtpResponse: action.payload, sendOtpisLoading: false, sendOtpisSuccess: false }

        case OTP_USER:

            return {
                ...state, sendOtpResponse: '', sendOtpisLoading: true, sendOtpisSuccess: false,

            }




        default:
            return state;

    }

};