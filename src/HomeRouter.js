import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { View, StyleSheet, Easing, Animated, Dimensions } from "react-native";
//  import DashBoard from "./components/home/dashBoard";
import SideMenu from "./SideMenu";
import { createAppContainer, StackNavigator, DrawerNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { colors } from './theme';
import { createStackNavigator } from 'react-navigation-stack'
import Dashboard from './components/home/dashBoard'
import BarDetails from './components/home/barDetail'
import UserList from './components/home/userList'
import ChatScreen from './components/home/ChatScreen'
import OtherProfileDetails from './components/home/OtherProfileDetails'
import MyProfile from './components/home/myProfile'

// import Iap from './components/home/Iap'

import Pricing from './components/home/PlanList'
import Privacy from './components/home/PrivacyPolicy'
import TermCondition from './components/home/TermCondition'
import Friends from './components/home/Friends'
import AddMembers from './components/home/Addmembers'
import MyRegistration from './components/home/MyRegistrations'
import ChangePassword from './components/home/ChangePassword'
import PairUp from './components/home/PairUp'
import BlockList from './components/home/BlockList'
import Room from './components/home/Room'
import Settings from './components/home/Setting'
import Broadcast from './components/home/Broadcast'
import PrivacyTerms from './components/home/PrivacyTerms'
import ContactUs from './components/home/contactUs'
import RateShare from './components/home/RateShare'
import MyEntriesTab from './components/home/MyEntriesTab'
const DEVICE_WIDTH = Dimensions.get("window").width;
const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { position, layout, scene, index, scenes } = sceneProps
      const toIndex = index
      const thisSceneIndex = scene.index
      const height = layout.initHeight
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [width, 0, 0]
      })

      // Since we want the card to take the same amount of time
      // to animate downwards no matter if it's 3rd on the stack
      // or 53rd, we interpolate over the entire range from 0 - thisSceneIndex
      const translateY = position.interpolate({
        inputRange: [0, thisSceneIndex],
        outputRange: [height, 0]
      })

      const slideFromRight = { transform: [{ translateX }] }
      const slideFromBottom = { transform: [{ translateY }] }

      const lastSceneIndex = scenes[scenes.length - 1].index

      // Test whether we're skipping back more than one screen
      if (lastSceneIndex - toIndex > 1) {
        // Do not transoform the screen being navigated to
        if (scene.index === toIndex) return
        // Hide all screens in between
        if (scene.index !== lastSceneIndex) return { opacity: 0 }
        // Slide top screen down
        return slideFromBottom
      }

      return slideFromRight
    },
  }
}


const routeConfig = {
  BarDetails: { screen: BarDetails, navigationOptions: { header: null } },
  UserList: { screen: UserList, navigationOptions: { header: null } },
  OtherProfileDetails: { screen: OtherProfileDetails, navigationOptions: { header: null } },
  ChatScreen: { screen: ChatScreen, navigationOptions: { header: null } },
  MyProfile: { screen: MyProfile, navigationOptions: { header: null } },

  // Iap: { screen: Iap, navigationOptions: { header: null } },
  // Packages: { screen: Packages, navigationOptions: { header: null } },
  Privacy: { screen: Privacy, navigationOptions: { header: null },path: 'chat/:user' },
  TermCondition: { screen: TermCondition, navigationOptions: { header: null } },
  Friends: { screen: Friends, navigationOptions: { header: null } },
  PairUp: { screen: PairUp, navigationOptions: { header: null } },
  AddMembers: { screen: AddMembers, navigationOptions: { header: null } },
  MyRegistration: { screen: MyRegistration, navigationOptions: { header: null } },
  ChatScreen: { screen: ChatScreen, navigationOptions: { header: null } },
  ChangePassword: { screen: ChangePassword, navigationOptions: { header: null } },
  BlockList: { screen: BlockList, navigationOptions: { header: null } },
  Room: { screen: Room, navigationOptions: { header: null } },
  Settings: { screen: Settings, navigationOptions: { header: null } },
  Broadcast: { screen: Broadcast, navigationOptions: { header: null } },
  PrivacyTerms: { screen: PrivacyTerms, navigationOptions: { header: null } },
  Packages: { screen: Pricing, navigationOptions: { header: null } },
  ContactUs: { screen: ContactUs, navigationOptions: { header: null } },
  RateShare: { screen: RateShare, navigationOptions: { header: null } },
  MyEntriesTab: { screen: MyEntriesTab, navigationOptions: { header: null } },
  Drawer: {
    screen: createDrawerNavigator({
      Dashboard: {
        screen: Dashboard
      },
    },
    
    
    {
        // gesturesEnabled: true,
        unmountInactiveRoutes: true,
        drawerType: 'Slide',
        contentComponent: SideMenu,
        drawerWidth: DEVICE_WIDTH * .7,
        // initialRouteName: this.props.gotoPriend?'Dashboard':'Dashboard',
      })
  },

}
const DrawerNavigation = createStackNavigator(
  routeConfig, {
    initialRouteName: 'Drawer',
    headerMode: 'none',
    transitionConfig,
  }
)


const App = createAppContainer(DrawerNavigation);
class HomeRouter extends React.Component {
  render() {
    return (
      <App />
    )
  }
}


export default App; 