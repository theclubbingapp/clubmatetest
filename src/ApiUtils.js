import RNRestart from 'react-native-restart';
import {
  ScrollView,
  Image,
  Text,
  View, Alert,
  StyleSheet,
  Dimensions, LayoutAnimation,
  TouchableOpacity,
  Platform, AsyncStorage
} from "react-native";
import firebase from 'react-native-firebase';
var ApiUtils = {
  checkStatus: function (response) {
    console.log('all response data' + JSON.stringify(response))
    if (response.data.flag == 1) {
      return response;
    } else {
      if (response.data.msg == 400 || response.data.msg == '400') {
        AsyncStorage.clear();
        firebase.messaging().deleteToken()
        RNRestart.Restart();
        // if (!global.hit) {
        // global.hit = true;
        // Alert.alert(
        //   'Account Blocked',
        //   'your account is blocked please contact admin',
        //   [
        //     {
        //       text: 'OK', onPress: () => {

        //         AsyncStorage.clear();
        //         firebase.messaging().deleteToken()
        //         RNRestart.Restart();
        //       }
        //     },
        //   ],
        //   { cancelable: false },
        // );
        // }

      } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error;
      }

    }
  }
};


export { ApiUtils as default };