import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage,
    ScrollView,
    Platform,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import { CheckBox } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import back from "../../assets/back.png";
import chat from "../../assets/chat.png";
import block from "../../assets/block.png";
import other_profile from "../../assets/other_profile.png";
import BlueButton from "../common/BlueButton";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import ModalDatePicker from "react-native-datepicker-modal";
import Loading from 'react-native-whc-loading'
var imageName = '';
var imageValue = '';
var imageType = '';
var postData = '';
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {

            isEditable: false,
            isFromDrawer: false,
            loading: false,
            intrestedIn: [
                { id: 1, value: "Male", isChecked: false },
                { id: 2, value: "Female", isChecked: false },
                { id: 3, value: "Both", isChecked: false },

            ],
            dance: [
                { id: 1, value: "Dance", isChecked: false },
                { id: 2, value: "No-Dance", isChecked: false },
            ],
            maleFemale: [
                { id: 1, value: "Male", isChecked: false },
                { id: 2, value: "Female", isChecked: false },
            ],

            food: [
                { id: 1, value: "Veg", isChecked: false },
                { id: 2, value: "Non-Veg", isChecked: false },
            ],
            typesOfDrink: [
                { id: 1, value: "Brandy", isChecked: false },
                { id: 2, value: "Cachaça", isChecked: false },
                { id: 3, value: "Gin", isChecked: false },
                { id: 4, value: "Rum", isChecked: false },
                { id: 5, value: "Schnapps", isChecked: false },
                { id: 5, value: "Tequila", isChecked: false },
                { id: 5, value: "Vodka", isChecked: false },
                { id: 5, value: "Whisky", isChecked: false }
            ],
            bio: '',

            offeringADrink: [
                { id: 1, value: "Offering a Drink", isChecked: false },
            ],
            dateOfBirth: '',
            name: '',
            contact: '',
            mytoken: '',
            image: '',
            response: '',
            avatarSource: '',
        };



    }


    openCamera = () => {

        if (!this.state.isEditable) {
            return;
        }

        const options = {
            title: 'Select Image',
            quality: 0.5,
            maxWidth: 600,
            maxHeight: 600,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info in the API Reference)
         */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                var randomNumber = Math.floor((Math.random() * 100) + 1);
                imageName = 'image' + randomNumber + '.png';
                imageType = 'image/png';
                imageValue = response.uri;
                postData = {
                    uri: Platform.OS === "android" ? source.uri : source.uri.replace("file://", ""),
                    type: response.type,
                    name: imageName,
                    // filename: imageName,
                    // filetype: imageType,
                    // value: imageValue
                }
                // AsyncStorage.setItem("postImageData", JSON.stringify(postData));
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // console.log("postImageData.............................................", JSON.stringify(postData))
                this.setState({
                    avatarSource: source,
                });
            }
        });
    }


    updateProfile = () => {
        const formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('dob', this.state.dateOfBirth)
        formData.append('full_name', this.state.name)
        formData.append('gender', JSON.stringify(this.state.maleFemale))
        formData.append('offering_a_drink', JSON.stringify(this.state.offeringADrink))
        formData.append('food', JSON.stringify(this.state.food))
        formData.append('dance', JSON.stringify(this.state.dance))
        formData.append('type_of_drink', JSON.stringify(this.state.typesOfDrink))
        formData.append('intrested_in', JSON.stringify(this.state.intrestedIn))
        formData.append('bio', this.state.bio)
        formData.append('userfile', postData)

        const config = {
            headers: {
                'Content-Type': 'multipart/form-data',
                // 'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
            }
        };
        // formData.append('devicetoken', devicetoken)
        // formData.append('imei', imei)
        // formData.append('ostype', JSON.stringify(ostype))
        this.refs.loading.show();
       
        axios.post(APIURLCONSTANTS.UPDATEPROFILE, formData, config)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    loading: false
                })
                console.log('formData profile' + JSON.stringify(res))
                this.refs.loading.close();
                let mainData = res.data.msg;
                this.refs.refProfile.showMessage({
                    message: mainData,
                    type: "info",
                    position: 'bottom'
                });
                // console.log('res profile' + mainData.gender)
                // console.log('res profile' + JSON.parse(mainData))
                return;
                if (mainData != null && mainData != undefined) {

                    this.setState({
                        response: mainData
                    })
                    if (mainData.gender) {
                        this.setState({
                            maleFemale: JSON.parse(mainData.gender)
                        })
                    }

                    if (mainData.intrested_in) {
                        this.setState({
                            intrestedIn: JSON.parse(mainData.intrested_in)
                        })
                    }
                    if (mainData.offering_a_drink) {
                        this.setState({
                            offeringADrink: JSON.parse(mainData.offering_a_drink)
                        })
                    }
                    if (mainData.image) {
                        this.setState({
                            image: mainData.image
                        })
                    }
                    if (mainData.dance) {
                        this.setState({
                            dance: JSON.parse(mainData.dance)
                        })
                    }
                    if (mainData.food) {
                        this.setState({
                            food: JSON.parse(mainData.food)
                        })
                    }
                    if (mainData.type_of_drink) {
                        this.setState({
                            typesOfDrink: JSON.parse(mainData.type_of_drink)
                        })
                    }
                    if (mainData.dob) {
                        this.setState({
                            dateOfBirth: mainData.dob
                        })
                    }
                    if (mainData.full_name) {
                        this.setState({
                            name: mainData.full_name
                        })
                    }
                    if (mainData.bio) {
                        this.setState({
                            bio: mainData.bio
                        })
                    }
                    if (mainData.phone_number) {
                        this.setState({
                            contact: mainData.phone_number
                        })
                    }



                }

            })
            .catch(error => {
                this.refs.loading.close();
                this.setState({
                    loading: false
                })
                console.log('error profile' + JSON.stringify(error))
                if (error.response) {
                    this.refs.refRegister.showMessage({
                        message: error.response.data.msg,
                        type: "danger",
                        position: 'bottom'
                    });
                    // dispatch(verifyOtpFail(error.response.data.msg))
                } else if (error.request) {
                    this.refs.refRegister.showMessage({
                        message: error.request,
                        type: "danger",
                        position: 'bottom'
                    });

                } else {
                    this.refs.refRegister.showMessage({
                        message: error.message,
                        type: "danger",
                        position: 'bottom'
                    });


                }

            });
    }


    componentWillMount() {
        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })

                this.refs.loading.show();
                const formData = new FormData()
                formData.append('authtoken', value)
                console.log('formData profile' + JSON.stringify(formData))
                axios.post(APIURLCONSTANTS.GETPROFILE, formData, null)
                    .then(ApiUtils.checkStatus)
                    .then(res => {
                        this.setState({
                            loading: false
                        })
                        this.refs.loading.close();
                        let mainData = res.data.data;
                        // console.log('res profile' + mainData.gender)
                        console.log('res profile' + JSON.stringify(mainData))
                        if (mainData != null && mainData != undefined) {
                            if (mainData.gender) {
                                this.setState({
                                    maleFemale: JSON.parse(mainData.gender)
                                })
                            }

                            // if (mainData.gender) {
                            //     this.setState({
                            //         maleFemale:JSON.parse(mainData.offering_a_drink)
                            //     })
                            // }
                            if (mainData.intrested_in) {
                                this.setState({
                                    intrestedIn: JSON.parse(mainData.intrested_in)
                                })
                            }
                            if (mainData.image) {
                                this.setState({
                                    image: mainData.image
                                })
                            }

                            if (mainData.offering_a_drink) {
                                this.setState({
                                    offeringADrink: JSON.parse(mainData.offering_a_drink)
                                })
                            }
                            if (mainData.bio) {
                                this.setState({
                                    bio: mainData.bio
                                })
                            }
                            if (mainData.dance) {
                                this.setState({
                                    dance: JSON.parse(mainData.dance)
                                })
                            }
                            if (mainData.food) {
                                this.setState({
                                    food: JSON.parse(mainData.food)
                                })
                            }
                            if (mainData.type_of_drink) {
                                this.setState({
                                    typesOfDrink: JSON.parse(mainData.type_of_drink)
                                })
                            }
                            if (mainData.dob) {
                                this.setState({
                                    dateOfBirth: mainData.dob
                                })
                            }
                            if (mainData.full_name) {
                                this.setState({
                                    name: mainData.full_name
                                })
                            }
                            if (mainData.phone_number) {
                                this.setState({
                                    contact: mainData.phone_number
                                })
                            }



                        }

                    })
                    .catch(error => {
                        this.refs.loading.close();
                        this.setState({
                            loading: false
                        })
                        console.log('error profile' + JSON.stringify(error))

                        if (error.response) {
                            this.refs.refRegister.showMessage({
                                message: error.response.data.msg,
                                type: "danger",
                                position: 'bottom'
                            });
                            // dispatch(verifyOtpFail(error.response.data.msg))
                        } else if (error.request) {
                            this.refs.refRegister.showMessage({
                                message: error.request,
                                type: "danger",
                                position: 'bottom'
                            });

                        } else {
                            this.refs.refRegister.showMessage({
                                message: error.message,
                                type: "danger",
                                position: 'bottom'
                            });


                        }

                    });
            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }
        this.mounted = false



    }

    componentDidMount() {
        this.mounted = true



    }

    componentDidUpdate() { }

    handleCheckChieldElement = (name, isChecked) => {
        let typesOfDrink = this.state.typesOfDrink
        typesOfDrink.forEach(typesOfDrink => {
            if (typesOfDrink.value === name)
                typesOfDrink.isChecked = !isChecked
        })
        this.setState({ typesOfDrink: typesOfDrink })
    }

    handleIntrestedInElement = (name, isChecked) => {
        let intrestedIn = this.state.intrestedIn
        intrestedIn.forEach(item => {
            if (item.value === name)
                item.isChecked = !isChecked
        })
        this.setState({ intrestedIn: intrestedIn })
    }

    handleOfferingADrinkInElement = (name, isChecked) => {
        let offeringADrink = this.state.offeringADrink
        offeringADrink.forEach(item => {
            if (item.value === name)
                item.isChecked = !isChecked
        })
        this.setState({ offeringADrink: offeringADrink })
    }

    handleDanceElement = (name, isChecked) => {
        let dance = this.state.dance
        dance.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }
            this.setState({
                dance: name == "Dance" ? 1 : 2
            })

        })
        this.setState({ dance: dance })


    }

    handleFoodElement = (name, isChecked) => {
        let food = this.state.food
        food.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }

        })
        this.setState({ food: food })


    }

    handleMaleFemaleElement = (name, isChecked) => {
        let maleFemale = this.state.maleFemale
        maleFemale.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }

            // this.setState({
            //     gender: name == 'Male' ? 1 : 2
            // })

        })
        this.setState({ maleFemale: maleFemale })
    }


    chat = () => {
        const { navigation } = this.props;
        navigation.navigate('ChatScreen')
    }


    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };

    _handleDatePickedFrom = ({ date, year, day, month }) => {
        // alert(JSON.stringify(date))
        // console.log('date'+JSON.stringify(date))
        this.setState({
            dateOfBirth: day + "-" + month + "-" + year + "-"
        });
    };

    editProfile = () => {
        this.setState({
            isEditable: !this.state.isEditable
        })

        if (!this.state.isEditable) {
            if (this.state.response != null && this.state.response != undefined && this.state.response != '') {
                if (this.state.response.gender) {
                    this.setState({
                        maleFemale: JSON.parse(this.state.response.gender)
                    })
                }

                if (this.state.response.intrested_in) {
                    this.setState({
                        intrestedIn: JSON.parse(this.state.response.intrested_in)
                    })
                }
                if (this.state.response.image) {
                    this.setState({
                        image: this.state.response.image
                    })
                }
                if (this.state.response.dance) {
                    this.setState({
                        dance: JSON.parse(this.state.response.dance)
                    })
                }
                if (this.state.response.bio) {
                    this.setState({
                        bio: this.state.response.bio
                    })
                }
                if (this.state.response.food) {
                    this.setState({
                        food: JSON.parse(this.state.response.food)
                    })
                }
                if (this.state.response.type_of_drink) {
                    this.setState({
                        typesOfDrink: JSON.parse(this.state.response.type_of_drink)
                    })
                }
                if (this.state.response.offering_a_drink) {
                    this.setState({
                        offeringADrink: JSON.parse(this.state.response.offering_a_drink)
                    })
                }

                if (this.state.response.dob) {
                    this.setState({
                        dateOfBirth: this.state.response.dob
                    })
                }
                if (this.state.response.full_name) {
                    this.setState({
                        name: this.state.response.full_name
                    })
                }
                if (this.state.response.phone_number) {
                    this.setState({
                        contact: this.state.response.phone_number
                    })
                }
            }
        }



    }

    render() {
        let _this = this;
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text style={styles.header}>{this.state.name}</Text>
                        </View>
                        <TouchableOpacity onPress={this.updateProfile} style={styles.notiTouch}>
                            {this.state.isEditable ?
                                <Text style={styles.header}>
                                    Update
                            </Text>
                                :
                                null
                            }

                        </TouchableOpacity>
                    </View>
                    {/* <View>
                        <Image source={{ uri: 'https://placekitten.com/200/300' }}
                            style={styles.profileImage}
                        // resizeMode='contain'
                        />
                    </View> */}





                </View>
                <ScrollView style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })

                }}>

                    <TouchableOpacity onPress={this.openCamera}>
                        {this.state.avatarSource == '' ?
                            <Image source={{
                                uri: this.state.image + '?t=' + Math.round(new Date().getTime() / 1000), cache: 'reload'
                            }}
                                style={styles.profileImage}
                                key={(new Date()).getTime()}
                            />
                            :
                            <Image source={this.state.avatarSource}
                                style={styles.profileImage}
                                key={(new Date()).getTime()}
                            />}
                    </TouchableOpacity>
                    <View style={{ marginTop: DEVICE_HEIGHT * .02, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Name</Text>

                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'flex-start' }} >
                                <TextInput
                                    // keyboardType='phone-pad'
                                    editable={this.state.isEditable}
                                    //   selectTextOnFocus={!isAuthenticating}
                                    // keyboardType='phone-pad'
                                    onChangeText={(name) => this.setState({ name: String.prototype.trim.call(name) })}
                                    style={styles.inputLayout}
                                    placeholder={"Name"}
                                    value={this.state.name}
                                    autoCorrect={false}
                                    autoCapitalize={"none"}
                                    returnKeyType={"done"}
                                    placeholderTextColor={colors.gray}
                                    underlineColorAndroid="transparent"
                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>
                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Contact No.</Text>

                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'flex-start' }} >
                                <TextInput
                                    // keyboardType='phone-pad'
                                    editable={this.state.isEditable}
                                    //   selectTextOnFocus={!isAuthenticating}
                                    // keyboardType='phone-pad'
                                    onChangeText={(contact) => this.setState({ contact: String.prototype.trim.call(contact) })}
                                    style={styles.inputLayout}
                                    placeholder={"Contact"}
                                    value={this.state.contact}
                                    autoCorrect={false}
                                    autoCapitalize={"none"}
                                    returnKeyType={"done"}
                                    placeholderTextColor={colors.gray}
                                    underlineColorAndroid="transparent"
                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                    </View>
                    <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Date of Birth</Text>

                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'flex-start' }} >

                                <ModalDatePicker
                                    maxDate={new Date()}
                                    style={styles.modelDatePickerStyle}
                                    renderDate={({ year, month, day, date }) => {
                                        if (!date) {
                                            return (
                                                <Text style={styles.modelDatePickerPlaceholderStyle}>
                                                    {this.state.dateOfBirth}
                                                </Text>
                                            );
                                        }

                                        const dateStr = `${day}-${month}-${year}`;
                                        return (
                                            <Text style={{
                                                fontFamily: fonts.normal,
                                                fontSize: (DEVICE_HEIGHT * 0.027 * .8),
                                                height: DEVICE_HEIGHT * 0.06,
                                                color: colors.black
                                            }}>
                                                {dateStr}
                                            </Text>
                                        );
                                    }}
                                    onDateChanged={this._handleDatePickedFrom}
                                // startDate={this.state.startDateFrom}
                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                    </View>


                    <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Gender</Text>

                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center' }}>


                                <FlatList
                                    style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                                    contentContainerStyle={{}}
                                    data={this.state.maleFemale}
                                    renderItem={({ item }) => (
                                        <CheckBox
                                            // handleCheckChieldElement={this.handleCheckChieldElement}
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            title={item.value}
                                            containerStyle={{ width: (DEVICE_WIDTH * .85) / 4, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                            checked={item.isChecked}
                                            checkedColor={colors.primaryColor}
                                            onPress={() => { this.state.isEditable ? this.handleMaleFemaleElement(item.value, item.isChecked) : null }}
                                            size={DEVICE_HEIGHT * 0.02}
                                            textStyle={[styles.modelDatePickerStringStyle,]}



                                        // {...typesOfDrink} 
                                        />
                                    )}
                                    extraData={this.state}
                                    horizontal={true}
                                    //Setting the number of column
                                    // numColumns={3}
                                    keyExtractor={(item, index) => index}

                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                    </View>


                    <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Food</Text>

                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center' }}>


                                <FlatList
                                    style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                                    contentContainerStyle={{}}
                                    data={this.state.food}
                                    renderItem={({ item }) => (
                                        <CheckBox
                                            // handleCheckChieldElement={this.handleCheckChieldElement}
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            title={item.value}
                                            containerStyle={{ width: (DEVICE_WIDTH * .85) / 4, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                            checked={item.isChecked}
                                            checkedColor={colors.primaryColor}
                                            onPress={() => { this.state.isEditable ? this.handleFoodElement(item.value, item.isChecked) : null }}
                                            size={DEVICE_HEIGHT * 0.02}
                                            textStyle={[styles.modelDatePickerStringStyle,]}



                                        // {...typesOfDrink} 
                                        />
                                    )}
                                    extraData={this.state}
                                    horizontal={true}
                                    //Setting the number of column
                                    // numColumns={3}
                                    keyExtractor={(item, index) => index}

                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                    </View>
                    <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * 0.027, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Dance</Text>

                            </View>

                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center' }}>


                                <FlatList
                                    style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                                    contentContainerStyle={{}}
                                    data={this.state.dance}
                                    renderItem={({ item }) => (
                                        <CheckBox
                                            // handleCheckChieldElement={this.handleCheckChieldElement}
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            title={item.value}
                                            containerStyle={{ width: (DEVICE_WIDTH * .85) / 4, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                            checked={item.isChecked}
                                            checkedColor={colors.primaryColor}
                                            onPress={() => { this.state.isEditable ? this.handleDanceElement(item.value, item.isChecked) : null }}
                                            size={DEVICE_HEIGHT * 0.02}
                                            textStyle={[styles.modelDatePickerStringStyle,]}



                                        // {...typesOfDrink} 
                                        />
                                    )}
                                    extraData={this.state}
                                    horizontal={true}
                                    //Setting the number of column
                                    // numColumns={3}
                                    keyExtractor={(item, index) => index}

                                />
                            </View>


                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                    </View>

                    <View style={{ paddingHorizontal: DEVICE_WIDTH * 0.027, alignSelf: 'center', marginTop: DEVICE_HEIGHT * 0.027, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Type of Drink
                                </Text>

                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={this.state.typesOfDrink}
                            renderItem={({ item }) => (
                                <CheckBox
                                    handleCheckChieldElement={this.handleCheckChieldElement}
                                    title={item.value}
                                    containerStyle={{ width: (DEVICE_WIDTH * .85) / 3, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => { this.state.isEditable ? this.handleCheckChieldElement(item.value, item.isChecked) : null }}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}



                                // {...typesOfDrink} 
                                />
                            )}
                            extraData={this.state}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index}

                        />


                    </View>
                    <View style={{ paddingLeft: DEVICE_WIDTH * 0.027, marginTop: DEVICE_HEIGHT * 0.027, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Intrested In
                                </Text>

                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={this.state.intrestedIn}
                            renderItem={({ item }) => (
                                <CheckBox
                                    handleCheckChieldElement={this.handleCheckChieldElement}
                                    title={item.value}
                                    containerStyle={{ width: (DEVICE_WIDTH * .85) / 3, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => { this.state.isEditable ? this.handleIntrestedInElement(item.value, item.isChecked) : null }}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}



                                // {...typesOfDrink} 
                                />
                            )}
                            extraData={this.state}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index}

                        />
                    </View>

                    <View style={{ paddingLeft: DEVICE_WIDTH * 0.027, marginTop: DEVICE_HEIGHT * 0.027, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Drink
                                </Text>

                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={this.state.offeringADrink}
                            renderItem={({ item }) => (
                                <CheckBox
                                    handleCheckChieldElement={this.handleCheckChieldElement}
                                    title={item.value}
                                    containerStyle={{ width: (DEVICE_WIDTH * .85) / 3, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => { this.state.isEditable ? this.handleOfferingADrinkInElement(item.value, item.isChecked) : null }}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}



                                // {...typesOfDrink} 
                                />
                            )}
                            extraData={this.state}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index}

                        />
                    </View>

                    <View>
                        <TextInput
                            editable={this.state.isEditable}
                            onChangeText={(bio) => this.setState({ bio: String.prototype.trim.call(bio) })}
                            style={styles.textInput}
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                            value={this.state.bio}
                            selectionColor={colors.primaryColor}
                            placeholderTextColor={colors.textColor}
                            // keyboardType="email-address"
                            returnKeyType="default"
                            multiline={true}
                            placeholder="Bio"
                        />

                    </View>


                    <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                        {this.state.isEditable ?
                            <BlueButton
                                onPress={this.editProfile}
                                style={{
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                CANCEL
                                      </BlueButton>

                            :
                            <BlueButton
                                onPress={this.editProfile}
                                style={{
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                EDIT PROFILE
                                      </BlueButton>

                        }
                    </View>


                </ScrollView>
                <Loading ref="loading"
                    backgroundColor='#ffffffF2'
                    borderRadius={5}
                    size={70}
                    imageSize={40}
                    indicatorColor='gray'
                />

                <FlashMessage ref="refProfile" />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    textInput: {
        paddingHorizontal: DEVICE_HEIGHT * 0.01,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: colors.gray,
        textAlignVertical: "top",
        marginTop: DEVICE_HEIGHT * 0.04,
        alignSelf: "center",
        width: DEVICE_WIDTH * 0.85,
        height: DEVICE_HEIGHT * 0.15,
        // backgroundColor: colors.inputField,
        fontSize: DEVICE_HEIGHT * 0.02,
        fontFamily: fonts.normal,
        color: colors.inputText
    },


    labelText: { fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * 0.027 * .8), alignSelf: 'flex-start' },

    otherProfile: {
        height: DEVICE_HEIGHT * 0.2 * .3,
        width: DEVICE_HEIGHT * 0.2 * .3,
        borderRadius: (DEVICE_HEIGHT * 0.2) * .3 / 2,
        alignSelf: 'center',

    },


    profileImage: {
        // position: 'absolute',
        ...ifIphoneX(
            {
                marginTop: DEVICE_HEIGHT * .02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                marginTop:
                    Platform.OS == "ios"
                        ? DEVICE_HEIGHT * .02
                        : DEVICE_HEIGHT * .02
            }
        ),
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.2 * .7,
        width: DEVICE_HEIGHT * 0.2 * .7,
        borderRadius: (DEVICE_HEIGHT * 0.2) * .7 / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.textColor,
    },
    modelDatePickerPlaceholderStyle: {
        // fontFamily: fonts.normal,
        // fontSize: DEVICE_HEIGHT * 0.02,
        // color: colors.gray,

        fontFamily: fonts.normal,
        fontSize: (DEVICE_HEIGHT * 0.027 * .8),
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },

    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.06

    },

    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027
    },


    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: (DEVICE_HEIGHT * 0.027 * .8),
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },

    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyProfile);
