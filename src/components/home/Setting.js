import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard, Switch,
    Alert,
    FlatList,
    AsyncStorage,
    ScrollView, Slider,
    Platform, ActivityIndicator,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import BlueButton from '../common/BlueButton'
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            mobileRecharge: [],
            page: 1,
            refreshing: false,
            sliderValue: 10,
            switchValue: false,
            isLoadingButton: false,
            error: ''
        };
    }

    change(value) {
        this.setState(() => {
            return {
                sliderValue: parseFloat(value),
            };
        });
    }
    onClickBack = () => {
        this.props.navigation.goBack();
    };
    componentWillMount() {
        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })
                const formData = new FormData()
                formData.append('authtoken', value)
                console.log('formData profile' + JSON.stringify(formData))
                this.setState({
                    loading: true
                })

                axios.post(APIURLCONSTANTS.GET_RADIUS_SETTING, formData, null)
                    .then(ApiUtils.checkStatus)
                    .then(res => {
                        this.setState({
                            loading: false,
                            error: ''
                        })
                        let data = res['data']['data']
                        console.log(JSON.stringify(data))
                        if (data != null && data != "") {
                            this.setState({
                                sliderValue: data[0]['radios_range']!=''?parseInt(data[0]['radios_range']):0,
                                switchValue: data[0]['notification_status'] == '1' ? true : false
                            })
                        }
                    })
                    .catch(error => {
                        this.refs.loading.close();
                        this.setState({
                            loading: false,
                            error: 'error'
                        })
                    });
            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }
    }

    componentDidMount() {


    }

    retry = () => {
        const formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        console.log('formData profile' + JSON.stringify(formData))
        this.setState({
            loading: true
        })
        axios.post(APIURLCONSTANTS.GET_RADIUS_SETTING, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    loading: false,
                    error: ''
                })
                let data = res['data']['data']
                if (data != null && data != "") {
                    this.setState({
                        sliderValue: parseInt(data[0]['radios_range']),
                        switchValue: parseInt(data[0]['notification_status'] == 1 ? true : false)
                    })
                }
            })
            .catch(error => {
                this.refs.loading.close();
                this.setState({
                    loading: false,
                    error: 'error'
                })
            });
    }

    componentDidUpdate() { }




    openDrawer = () => {
        this.props.navigation.openDrawer();
    };

    toggleSwitch = (value) => {
        this.setState({ switchValue: value })
        console.log('Switch 1 is: ' + value)
    }


    onSubmit = () => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('notification_status', this.state.switchValue ? 1 : 2)
               formData.append('radios_range', this.state.sliderValue)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        this.setState({
            isLoadingButton: true
        })
        axios.post(APIURLCONSTANTS.RADIUS_SETTING, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoadingButton: false
                })
                Alert.alert('Setting updated')
            })
            .catch(error => {
                this.setState({
                    isLoadingButton: false
                })
            });
    }

    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text  allowFontScaling={false} style={styles.header}>Settings</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{
                    flex: 1, ...ifIphoneX({

                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>
                    {this.state.loading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <ActivityIndicator
                                style={{ alignSelf: 'center' }}
                                size={'large'}
                                color={colors.primaryColor}
                            ></ActivityIndicator>
                        </View> :
                        <View style={{
                            flex: 1, ...ifIphoneX({
                                paddingBottom: getBottomSpace()
                            }, {
                                    paddingBottom: getBottomSpace()

                                })
                        }}>
                            {this.state.error != '' ?
                                <View style={{
                                    flex: 1,
                                    alignSelf: 'center',
                                    justifyContent: 'center',
                                    alignSelf: 'center'
                                }}>
                                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', color: colors.primaryColor, fontSize: DEVICE_HEIGHT * 0.027 }}>
                                            No setting's found!
                                       </Text>
                                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .05 }}>
                                            Retry
                                  </Text>
                                    </TouchableOpacity>
                                </View> :

                                <View>


                                    <View>
                                        <Text  allowFontScaling={false} style={{ marginLeft: DEVICE_WIDTH * .10, marginTop: DEVICE_HEIGHT * 0.027, fontSize: DEVICE_HEIGHT * 0.027, color: colors.textColor, fontFamily: fonts.bold }}>Broadcast Distance</Text>

                                        <Text  allowFontScaling={false} style={{ marginTop: DEVICE_HEIGHT * 0.027, alignSelf: 'center', fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontFamily: fonts.bold }}>
                                            {String(this.state.sliderValue) + ' Km'}
                                        </Text>
                                        <Slider
                                            style={{ width: DEVICE_WIDTH * .85, alignSelf: 'center' }}
                                            step={1}
                                            maximumValue={50}
                                            minimumTrackTintColor={colors.primaryColor}
                                            maximumTrackTintColor={colors.gray}
                                            thumbTintColor={colors.primaryColor}
                                            onValueChange={this.change.bind(this)}
                                            value={this.state.sliderValue}
                                        />
                                    </View>
                                    <View style={{ marginTop: DEVICE_HEIGHT * 0.027, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', marginLeft: DEVICE_WIDTH * .10, fontSize: DEVICE_HEIGHT * 0.027, color: colors.textColor, fontFamily: fonts.bold }}>Broadcast Notifcation</Text>
                                        <View style={{ flexDirection: 'row', }}>
                                            <Text  allowFontScaling={false} style={{ alignSelf: 'center', fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontFamily: fonts.bold }}>
                                                {this.state.switchValue ? 'On' : 'Off'}
                                            </Text>
                                            <Switch
                                                style={{ marginRight: DEVICE_WIDTH * .10, alignSelf: 'center', marginLeft: DEVICE_WIDTH * .10, }}
                                                onValueChange={this.toggleSwitch}
                                                value={this.state.switchValue} />
                                        </View>
                                    </View>

                                    <View style={{ marginTop: DEVICE_HEIGHT * .09, marginBottom: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                                        {this.state.isLoadingButton ?

                                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                                <ActivityIndicator color={colors.primaryColor} />
                                            </View>

                                            :
                                            <BlueButton

                                                onPress={this.onSubmit}
                                                // onPress={this.handleJoin}
                                                style={{
                                                    borderRadius:5,
                                                    backgroundColor: colors.primaryColor,
                                                    width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                }}
                                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                SUBMIT
                         </BlueButton>

                                        }


                                    </View>

                                </View>

                            }
                        </View>


                    }
                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);
