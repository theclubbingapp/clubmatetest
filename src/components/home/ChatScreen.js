import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  BackHandler,
  Keyboard,
  Alert,
  FlatList,
  AsyncStorage, ActivityIndicator,
  ScrollView, Slider,
  Platform, LayoutAnimation,
  ImageBackground, KeyboardAvoidingView,
  Dimensions, UIManager
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import back from "../../assets/back.png";
import {
  getStatusBarHeight,
  getBottomSpace,
  ifIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "../../theme";
import { GiftedChat, Actions, Bubble, SystemMessage } from 'react-native-gifted-chat';
import CustomActions from './CustomActions';
import CustomView from './CustomView';
import list from "../../assets/list.png";
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import firebaseService from '../../../src/service/firebase'
import { getChatItems } from './selectors'
import BlueButton from "../common/BlueButton";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import { TextInputLayout } from 'rn-textinputlayout';
// import Dialog from "react-native-dialog";
import Dialog, { DialogTitle, DialogFooter, ScaleAnimation, SlideAnimation, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { GlobalClass } from '../../GlobalClass.js';
export default class ChatScreen extends React.Component {
  _menu = null;
  constructor(props) {
    super(props);
    this.state = {
      showReport: false,
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      authtoken: this.props.navigation.state.params['authtoken'],
      from: this.props.navigation.state.params['from'],
      mytoken: this.props.navigation.state.params['mytoken'],
      device_token: this.props.navigation.state.params['device_token'],
      // authtoken: 123,
      // mytoken: 456,
      name: this.props.navigation.state.params['name'],
      messageId: '',
      myImage: this.props.navigation.state.params['myImage'],
      otherUserImage: this.props.navigation.state.params['otherUserImage'],
      phone_number: this.props.navigation.state.params['phone_number'],
      myPhoneNumber: this.props.navigation.state.params['myPhoneNumber'],
      myName: '',
      fcmToken: '',

      title: '',
      description: '',
      isFirst: true,
      pairup_status: 5,
      offer_status: 7,
      event_id: this.props.navigation.state.params['event_id'],

      sendFriendRequsetLoading: false,
      acceptRequestLoading: false,
      rejectRequestLoading: false,
      acceptDrinkRequestLoading: false,
      rejectDrinkRequestLoading: false,
      removeFriendLoading: false,
      cancelRequestLoading: false,
      sendDrinkRequestLoading: false,

    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderSystemMessage = this.renderSystemMessage.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);
    this.renderCustomView = this.renderCustomView.bind(this);

    this._isAlright = null;
    this.getName();
  }

  async getName() {
    let myName = await AsyncStorage.getItem(keys.username);
    this.setState({
      myName: myName
    })
    console.log('my name===' + this.state.myImage)


  }

  parse = snapshot => {
    const { text, user, createdAt } = snapshot.val();
    const { key: _id } = snapshot;
    // const timestamp = new Date(numberStamp);
    const message = {
      _id,
      // timestamp,
      createdAt: JSON.parse(createdAt),
      text: text == 'has requested you to join Call' ? user._id == this.state.mytoken ? 'You have requested ' + this.state.name + ' to join call' : this.state.name + ' has requested you to join call' : text,
      user,
    };
    return message;
  };

  async getToken() {
    let fcmToken = await AsyncStorage.getItem(keys.fcmToken);
    this.setState({
      fcmToken: fcmToken
    })
  }

  componentWillMount() {
    this.setState({
      isFirst: true
    })
    this.getToken();
    this.checkPairUp();
    this.checkOffer();
    this._isMounted = true;
    var myPhone = this.state.myPhoneNumber.slice(1);
    var phone = this.state.phone_number.slice(1);
    if (myPhone > phone) {
      this.state.messageId = myPhone + "" + phone
    } else {
      this.state.messageId = phone + "" + myPhone
    }
    // console.log('this.state.messageId' + this.state.messageId)
    firebaseService.database().ref('Messages/' + this.state.messageId).on('child_added', (snapshot) => {
      // const data = getChatItems(snapshot.val()).reverse();
      // console.log('retrive chats' + JSON.stringify(snapshot))
      let data = this.parse(snapshot);

      this.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, data),

        };
      });
    }, (errorObject) => {

    })

    // firebaseService.database().ref('Messages/' + this.state.messageId).on('value', (snapshot) => {
    //   // const data = getChatItems(snapshot.val()).reverse();
    //   console.log('retrive childSnapshot' + JSON.stringify(snapshot))
    //   const data = getChatItems(snapshot.val()).reverse();

    //   var items = [];
    //   snapshot.forEach((child) => {
    //     items.push(child);
    //   });


    // console.log('retrive childSnapshot' + JSON.stringify(prevChildKey))
    // let newMessage = []
    // newMessage.push(childSnapshot)
    // this.setState((previousState) => {
    //   return {
    //     // messages: GiftedChat.append(previousState.messages, snapshot),

    //   };
    // });
    // }, (errorObject) => {

    // })

  }



  blockUser = () => {
    Alert.alert(
      'Block',
      'Are you sure?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Block', onPress: () =>

            this._block()
        },
      ],
      { cancelable: false },
    );
  }

  _block = () => {
    let formData = new FormData()
    formData.append('authtoken', this.state.mytoken)
    formData.append('friend_authtoken', this.state.authtoken)
    console.log('_block dtaa' + JSON.stringify(formData))
    axios.post(APIURLCONSTANTS.BLOCK_USER, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.props.navigation.goBack()
      })
      .catch(error => {
        Alert.alert('Block', error['response']['data']['msg'])
      });
  }

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
    this.setState({
      showReport: true
    })

  };


  call = () => {
    // 
    let form = new FormData();
    form.append('authtoken', this.state.mytoken)
    form.append('friend_authtoken', this.state.authtoken)
    axios.post(APIURLCONSTANTS.PREMIUM_INSERT_CALL_CHECK, form, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        console.log('respoonse json' + JSON.stringify(res))
        let chatStatus = res['data']['call_status'];
        console.log('chatStatus' + chatStatus)
        if (chatStatus == 1 || chatStatus == '1') {
          let messages = [{
            text: 'has requested you to join Call',
            user: { _id: this.state.mytoken, name: this.state.myPhoneNumber },
            createdAt: JSON.stringify(new Date()),
            type: 'call',
            _id: JSON.stringify(new Date().toDateString())
          }]
          this._menu.hide();
          messages[0].createdAt = JSON.stringify(new Date());
          firebaseService.database().ref('Messages/' + this.state.messageId).push().set(...messages, (error) => {
            if (error) {
              // dispatch(chatMessageError(error.message))
            } else {
              AsyncStorage.getItem(keys.fcmToken).then((token) => {
                this.setState({
                  fcmToken: token
                });
              });
              let formData =
              {
                "to": this.state.device_token,
                "notification": {
                  "title": this.state.myName,
                  "body": 'has requested you to join Call',
                  'tag':'chat',
                  // "badge": this.state.mytoken,
                  "mutable_content": true,
                  "sound": "Tri-tone"
                },
                "data": {
                  "type": "chat",
                  "phone_number": this.state.phone_number,
                  "myPhoneNumber": this.state.myPhoneNumber,
                  "myImage": this.state.myImage,
                  "otherUserImage": this.state.otherUserImage,
                  "name": this.state.myName,
                  "authtoken": this.state.authtoken,
                  "device_token": this.state.fcmToken,
                  "event_id": this.state.event_id,
                  "myToken": this.state.mytoken,
                }
              }
              let config = {
                headers: {
                  "Content-Type": "application/json",
                  "Authorization": APIURLCONSTANTS.FIREBASE_KEY
                }

              }
              axios.post('https://fcm.googleapis.com/fcm/send', formData, config)
                .then(ApiUtils.checkStatus)
                .then(res => {

                })
                .catch(error => {
                  console.log('response error' + JSON.stringify(error))
                  this.setState({
                    isLoading: false
                  })

                });

            }
          })

        } else {
          Alert.alert(
            'Info',
            'Please subscribe to premium plan',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Proceed', onPress: () => { this.props.navigation.navigate('Packages'), this._menu.hide(); }


              },
            ],
            { cancelable: false },
          );
          // alert('Please subscribe to premium plan')
        }

      })
      .catch(error => {
        console.log('response error' + JSON.stringify(error))
      });


  }
  showMenu = () => {
    this._menu.show();
  };

  componentWillUnmount() {
    this._isMounted = false;
    global.isChatOpen = false;
    global.chatToken = '';
    GlobalClass.isChatOpen = false;
    GlobalClass.chatToken = '';
  }

  componentDidMount() {
    global.isChatOpen = true;
    global.chatToken = this.state.authtoken;
    GlobalClass.isChatOpen = true;
    GlobalClass.chatToken = this.state.authtoken;
  }

  onLoadEarlier() {
    this.setState((previousState) => {
      return {
        isLoadingEarlier: true,
      };
    });

    setTimeout(() => {
      if (this._isMounted === true) {
        this.setState((previousState) => {
          return {
            messages: GiftedChat.prepend(previousState.messages, require('./data/old_messages.js')),
            loadEarlier: false,
            isLoadingEarlier: false,
          };
        });
      }
    }, 1000); // simulating network
  }
  onClickBack = () => {
    global.isChatOpen = false;
    global.chatToken = '';
    GlobalClass.isChatOpen = false;
    GlobalClass.chatToken = '';
    this.props.navigation.goBack();
  }
  onSend(messages = []) {
    // messages[0].createdAt = JSON.stringify(new Date());
    // messages[0].type = 'chat';
    // console.log('messages' + JSON.stringify(messages))
    // firebaseService.database().ref('Messages/' + this.state.messageId).push().set(...messages, (error) => {
    //   if (error) {
    //     // dispatch(chatMessageError(error.message))
    //   } else {
    //     AsyncStorage.getItem(keys.fcmToken).then((token) => {
    //       this.setState({
    //         fcmToken: token
    //       });
    //     });
    //     let formData =
    //     {
    //       "to": this.state.device_token,
    //       "notification": {
    //         "title": this.state.myName,
    //         "body": messages[0].text,
    //         "mutable_content": true,
    //         "sound": "Tri-tone"
    //       },


    //       "data": {
    //         "type": "chat",
    //         "phone_number": this.state.phone_number,
    //         "myPhoneNumber": this.state.myPhoneNumber,
    //         "myImage": this.state.myImage,
    //         "otherUserImage": this.state.otherUserImage,
    //         "name": this.state.myName,
    //         "authtoken": this.state.authtoken,
    //         "device_token": this.state.fcmToken,
    //         "event_id": this.state.event_id,
    //       }
    //     }

    //     let config = {
    //       headers: {
    //         "Content-Type": "application/json",
    //         "Authorization": APIURLCONSTANTS.FIREBASE_KEY
    //       }

    //     }
    //     axios.post('https://fcm.googleapis.com/fcm/send', formData, config)
    //       .then(ApiUtils.checkStatus)
    //       .then(res => {

    //       })
    //       .catch(error => {
    //         console.log('response error' + JSON.stringify(error))
    //         this.setState({
    //           isLoading: false
    //         })

    //       });

    //   }
    // })
    // return;

    if (this.state.isFirst) {
      let form = new FormData();
      form.append('authtoken', this.state.mytoken)
      form.append('friend_authtoken', this.state.authtoken)
      axios.post(APIURLCONSTANTS.CHECKCANMESSAGE, form, null)
        .then(ApiUtils.checkStatus)
        .then(res => {
          let chatStatus = res['data']['chat_status'];
          console.log('chatStatus' + chatStatus)
          if (chatStatus == 1 || chatStatus == '1') {
            this.setState({
              isFirst: false,
            })
            messages[0].createdAt = JSON.stringify(new Date());
            messages[0].type = 'chat';
            firebaseService.database().ref('Messages/' + this.state.messageId).push().set(...messages, (error) => {
              if (error) {
                // dispatch(chatMessageError(error.message))
              } else {
                AsyncStorage.getItem(keys.fcmToken).then((token) => {
                  this.setState({
                    fcmToken: token
                  });
                });
                let formData =
                {
                  "to": this.state.device_token,
                  "notification": {
                    "title": this.state.myName,
                    "body": messages[0].text,
                    'tag':'chat',
                    // "badge": this.state.mytoken,
                    "mutable_content": true,
                    "sound": "Tri-tone"
                  },


                  "data": {
                    "type": "chat",
                    "phone_number": this.state.phone_number,
                    "myPhoneNumber": this.state.myPhoneNumber,
                    "myImage": this.state.myImage,
                    "otherUserImage": this.state.otherUserImage,
                    "name": this.state.myName,
                    "authtoken": this.state.authtoken,
                    "device_token": this.state.fcmToken,
                    "event_id": this.state.event_id,
                    "myToken": this.state.mytoken,
                  }
                }

                let config = {
                  headers: {
                    "Content-Type": "application/json",
                    "Authorization": APIURLCONSTANTS.FIREBASE_KEY
                  }

                }
                console.log('formData'+JSON.stringify(formData))
                axios.post('https://fcm.googleapis.com/fcm/send', formData, config)
                  .then(ApiUtils.checkStatus)
                  .then(res => {

                  })
                  .catch(error => {
                    console.log('response error' + JSON.stringify(error))
                    this.setState({
                      isLoading: false
                    })

                  });

              }
            })

          } else {
            Alert.alert(
              'Info',
              'Please subscribe to premium plan',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'Proceed', onPress: () =>

                    this.props.navigation.navigate('Packages')
                },
              ],
              { cancelable: false },
            );
            // alert('Please subscribe to premium plan')
          }


        })
        .catch(error => {
          console.log('response error' + JSON.stringify(error))
        });
    } else {
      messages[0].createdAt = JSON.stringify(new Date());
      messages[0].type = 'chat';
      console.log('messages' + JSON.stringify(messages))
      firebaseService.database().ref('Messages/' + this.state.messageId).push().set(...messages, (error) => {
        if (error) {
          // dispatch(chatMessageError(error.message))
        } else {
          AsyncStorage.getItem(keys.fcmToken).then((token) => {
            this.setState({
              fcmToken: token
            });
          });
          let formData =
          {
            "to": this.state.device_token,
            "notification": {
              "title": this.state.myName,
              "body": messages[0].text,
              'tag':'chat',
              // "badge": this.state.mytoken,
              "mutable_content": true,
              "sound": "Tri-tone"
            },


            "data": {
              "type": "chat",
              "phone_number": this.state.phone_number,
              "myPhoneNumber": this.state.myPhoneNumber,
              "myImage": this.state.myImage,
              "otherUserImage": this.state.otherUserImage,
              "name": this.state.myName,
              "authtoken": this.state.authtoken,
              "device_token": this.state.fcmToken,
              "event_id": this.state.event_id,
              "myToken": this.state.mytoken,
            }
          }

          let config = {
            headers: {
              "Content-Type": "application/json",
              "Authorization": APIURLCONSTANTS.FIREBASE_KEY
            }

          }
          console.log('formData'+JSON.stringify(formData))
          axios.post('https://fcm.googleapis.com/fcm/send', formData, config)
            .then(ApiUtils.checkStatus)
            .then(res => {

            })
            .catch(error => {
              console.log('response error' + JSON.stringify(error))
              this.setState({
                isLoading: false
              })

            });

        }
      })
    }







    // this.setState((previousState) => {
    //   return {
    //     messages: GiftedChat.append(previousState.messages, messages),
    //   };
    // });

    // messages: GiftedChat.append(previousState.messages, {
    //   text: text,
    //   createdAt: new Date(),
    //   user: {
    //     _id: 2,
    //     name: 'React Native',
    //     avatar: 'https://placekitten.com/200/300',
    //   },
    // }),


    // for demo purpose
    // this.answerDemo(messages);
  }

  answerDemo(messages) {
    if (messages.length > 0) {
      if ((messages[0].image || messages[0].location) || !this._isAlright) {
        this.setState((previousState) => {
          return {
            // typingText: 'React Native is typing'
          };
        });
      }
    }

    setTimeout(() => {
      if (this._isMounted === true) {
        if (messages.length > 0) {
          if (messages[0].image) {
            this.onReceive('Nice picture!');
          } else if (messages[0].location) {
            this.onReceive('My favorite place');
          } else {
            if (!this._isAlright) {
              this._isAlright = true;
              this.onReceive('Alright');
            }
          }
        }
      }

      this.setState((previousState) => {
        return {
          typingText: null,
        };
      });
    }, 1000);
  }

  onReceive(text) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: this.state.myImage,
          },
        }),
      };
    });
  }

  renderCustomActions(props) {
    if (Platform.OS === 'ios') {
      return (
        <CustomActions
          {...props}
        />
      );
    }
    const options = {
      'Action 1': (props) => {
        alert('option 1');
      },
      'Action 2': (props) => {
        alert('option 2');
      },
      'Cancel': () => { },
    };
    return (
      <Actions
        {...props}
        options={options}
      />
    );
  }

  // renderCustomView = (props) => {
  //   return (
  //     <Text>aaaa</Text>

  //   )
  // }

  renderBubble(props) {
    // console.log('props====' + JSON.stringify(props.currentMessage.text))
    return (
      <Bubble
        messageRead={false}
        messageDelivered={false}
        {...props}

        textStyle={{
          right: {
            color: 'white',
            fontFamily: fonts.normal
          },
          left: {
            color: colors.primaryColor,
            fontFamily: fonts.normal
          }
        }}
        wrapperStyle={{
          left: {
            backgroundColor: '#f0f0f0',
          },
          right: {
            backgroundColor: colors.primaryColor
          }
        }}
      />
    );


  }
  renderAvtar = () => {
    <View style={{ height: 0, width: 0 }}>
    </View>
  }


  handleCancel = () => {
    this.setState({ showReport: false });
  };

  handleReport = () => {
    // authtoken
    // 5c8de89acdf2e15528039945

    // friend_id
    // 2

    // title
    // Test Report 

    // msg
    // hello this is  a ttest  report 
    this.removeFriend();

  };


  removeFriend = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('title', this.state.title)
    formData.append('msg', this.state.description)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    axios.post(APIURLCONSTANTS.REPORT, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
        this.setState({ showReport: false });
        Alert.alert(res['data']['msg'])

      })
      .catch(error => {
        // console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
        Alert.alert(error['response']['data']['msg'])
      });
  }

  renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 15,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  }

  checkPairUp = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', (this.state.event_id == null || this.state.event_id == 'null' || this.state.event_id == '') ? null : this.state.event_id)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    axios.post(APIURLCONSTANTS.CHECK_PAIRUP, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        // console.log('SEND_FRIEND_REQUEST res' + JSON.stringify(res))
        this.setState({
          pairup_status: res['data']['pairUp_status']

        })

      })
      .catch(error => {
        this.setState({
          pairup_status: 5
        })
      });
  }
  checkOffer = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', (this.state.event_id == null || this.state.event_id == 'null' || this.state.event_id == '') ? null : this.state.event_id)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    axios.post(APIURLCONSTANTS.CHECK_OFFER, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        console.log('SEND_FRIEND_REQUEST res' + JSON.stringify(res))
        this.setState({
          offer_status: res['data']['drink_status']

        })

      })
      .catch(error => {
        this.setState({
          offer_status: 7
        })
      });
  }

  pairUpRequest = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    this.setState({
      sendFriendRequsetLoading: true
    })
    axios.post(APIURLCONSTANTS.SEND_PAIRUP, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          pairup_status: 2
        })
        this.setState({
          sendFriendRequsetLoading: false
        })
        Alert.alert('Pair up request sent')
      })
      .catch(error => {
        this.setState({
          sendFriendRequsetLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }
  pairUpAcceptRequest = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      acceptRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.ACCEPT_PAIRUP, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          acceptRequestLoading: false
        })
        this.setState({
          pairup_status: 1
        })
        Alert.alert('Pair up request accepted')
      })
      .catch(error => {
        this.setState({
          acceptRequestLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }
  drinkOfferAcceptRequest = () => {
    let formData = new FormData()
    // formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      acceptDrinkRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.ACCEPT_DRINK, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          acceptDrinkRequestLoading: false
        })
        this.setState({
          offer_status: 1
        })
        Alert.alert('Drink request accepted')
      })
      .catch(error => {
        this.setState({
          acceptDrinkRequestLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }

  pairUpRejectRequest = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      rejectRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.REJECT_PAIRUP, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          rejectRequestLoading: false
        })
        this.setState({
          pairup_status: 2
        })
        Alert.alert('Pair up request rejected')
      })
      .catch(error => {
        this.setState({
          rejectRequestLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }
  drinkOfferRejectRequest = () => {
    let formData = new FormData()
    // formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      rejectDrinkRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.REJECT_DRINK, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          rejectDrinkRequestLoading: false
        })
        this.setState({
          offer_status: 2
        })
        Alert.alert('Drink request rejected')
      })
      .catch(error => {
        this.setState({
          rejectDrinkRequestLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }

  drinkSendRequest = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    // formData.append('drink_type', 'Requesting Drink')
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      sendDrinkRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.SEND_DRINK, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          sendDrinkRequestLoading: false
        })
        this.setState({
          offer_status: 3
        })
        Alert.alert('Drink request send')
      })
      .catch(error => {
        this.setState({
          sendDrinkRequestLoading: false
        })
        Alert.alert(error['response']['data']['msg'])
      });
  }

  pairUpCancelRequest = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      cancelRequestLoading: true
    })
    axios.post(APIURLCONSTANTS.CANCEL_PAIRUP_RERQUEST, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        this.setState({
          cancelRequestLoading: false
        })
        // console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
        Alert.alert('Pair up request cancelled')
        this.setState({
          pairup_status: 0

        })

      })
      .catch(error => {
        this.setState({
          cancelRequestLoading: false
        })
        console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
        Alert.alert(error['response']['data']['msg'])
      });
  }

  removePairUp = () => {
    let formData = new FormData()
    formData.append('friend_authtoken', this.state.authtoken)
    formData.append('authtoken', this.state.mytoken)
    formData.append('event_id', this.state.event_id)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    this.setState({
      removeFriendLoading: true
    })
    axios.post(APIURLCONSTANTS.DELETE_FRIEND, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        // console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
        this.setState({
          removeFriendLoading: false
        })
        Alert.alert('Pair up removed')
        this.setState({
          pairup_status: 0
        })

      })
      .catch(error => {
        this.setState({
          removeFriendLoading: false
        })
        // console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
        Alert.alert(error['response']['data']['msg'])
      });
  }

  callJoin = () => {
    alert('dgvga')
  }
  renderCustomView(props) {
    // console.log('props====' + JSON.stringify(props))
    return (
      <CustomView
        authtoken={this.state.authtoken}
        myToken={this.state.mytoken}
        myName={this.state.name}
        myImage={this.state.from == 'noti' ? this.state.myImage : this.state.otherUserImage}
        // myImage={this.state.myImage}
        callId={this.state.messageId}
        navigation={this.props.navigation}
        // call={this.callJoin}
        // onPress={() => this.callJoin()}
        position={props.position}
        containerStyle={props.containerStyle}
        text={props.currentMessage.text}

      // {...props}
      />
    );
  }
  // https://fcm.googleapis.com/fcm/send

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }

  render() {
    return (
      <View style={{
        ...ifIphoneX({
          paddingBottom: getBottomSpace()
        }, {
            paddingBottom: getBottomSpace()

          }), backgroundColor: colors.white, ...StyleSheet.absoluteFillObject,
      }}>

        <View style={styles.headerBackground}>
          <View style={styles.dashBoardRow}>
            {this.state.isFromDrawer ? (
              <TouchableOpacity
                onPress={this.openDrawer}
                style={styles.menuTouch}
              >
                <Image
                  source={menu}
                  style={styles.menuIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            ) : (
                <TouchableOpacity
                  onPress={this.onClickBack}
                  style={styles.menuTouch}
                >
                  <Image
                    source={back}
                    style={styles.menuIcon}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              )}
            <View style={styles.dashBoardView}>
              <Text style={styles.header}>{this.state.name}</Text>
            </View>
            <TouchableOpacity onPress={this.showMenu} style={styles.notiTouch}>
              <Image
                source={list}
                style={{
                  tintColor: colors.white,
                  height: DEVICE_HEIGHT * 0.05,
                  width: DEVICE_WIDTH * 0.05
                }}
                resizeMode="contain"
              />
              <Menu
                ref={this.setMenuRef}
              // button={<Text onPress={this.showMenu}>Show menu</Text>}
              >
                <MenuItem onPress={this.call}>Call</MenuItem>
                <MenuDivider />
                <MenuItem onPress={this.hideMenu}>Report</MenuItem>
                <MenuDivider />
                <MenuItem onPress={this.blockUser}>Block</MenuItem>
              </Menu>
            </TouchableOpacity>



          </View>

          <View style={{ marginBottom: DEVICE_HEIGHT * .01 }}>
            {this.state.pairup_status == 0 || this.state.pairup_status == '0' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                {this.state.sendFriendRequsetLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.pairUpRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    SEND PAIR UP
                              </BlueButton>
                }


              </View>
              :
              null}
            {this.state.pairup_status == 4 || this.state.pairup_status == '4' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                {this.state.acceptRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.pairUpAcceptRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    ACCEPT PAIR UP
              </BlueButton>
                }

                {this.state.rejectRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.pairUpRejectRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    REJECT PAIR UP
            </BlueButton>
                }
              </View>
              :
              null}
            {this.state.pairup_status == 1 || this.state.pairup_status == '1' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                {this.state.removeFriendLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.removePairUp}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    REMOVE PAIR UP
           </BlueButton>
                }
              </View> :
              null
            }

            {this.state.pairup_status == 2 || this.state.pairup_status == '2' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                {this.state.cancelRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.pairUpCancelRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    CANCEL PAIR UP
         </BlueButton>
                }


              </View>
              :
              null}
          </View>
          {/* // code for request drink */}

          <View style={{ marginBottom: DEVICE_HEIGHT * .01 }}>
            {/* {this.state.offer_status == 0 || this.state.offer_status == '0' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                {this.state.sendFriendRequsetLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.pairUpRequest}
                    style={{
                      backgroundColor: colors.white,
                      paddingHorizontal: DEVICE_WIDTH * .01,
                      // width: DEVICE_WIDTH * .35,
                      height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    REQUEST DRINK
                              </BlueButton>
                }


              </View>
              :
              null} */}
            {this.state.offer_status == 0 || this.state.offer_status == '0' ?
              <Text style={{ marginTop: DEVICE_HEIGHT * .02, alignSelf: 'center', fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                {this.state.name + ' has requested drinks'}
              </Text>
              :
              null}

            {/* $user_name has requested drinks */}

            {this.state.offer_status == 0 || this.state.offer_status == '0' ?
              <View style={{ marginTop: DEVICE_HEIGHT * .01, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                {this.state.acceptDrinkRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.drinkOfferAcceptRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      paddingHorizontal: DEVICE_WIDTH * .01,
                      // width: DEVICE_WIDTH * .35, 
                      height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    ACCEPT REQUEST
                    </BlueButton>
                }

                {this.state.rejectDrinkRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.drinkOfferRejectRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      // width: DEVICE_WIDTH * .35,
                      paddingHorizontal: DEVICE_WIDTH * .01,
                      height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    REJECT REQUEST
                  </BlueButton>
                }
              </View>
              :
              null}


            {this.state.offer_status == 4 || this.state.offer_status == '4' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  {this.state.name} has offered you drink
                    </Text>
              </View>
              :
              null}



            {this.state.offer_status == 3 || this.state.offer_status == '3' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  You requested for drink to {this.state.name}
                </Text>
              </View>
              :
              null}
            {this.state.offer_status == 2 || this.state.offer_status == '2' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  You rejected drink request from {this.state.name}
                </Text>
              </View>
              :
              null}
            {this.state.offer_status == 1 || this.state.offer_status == '1' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ alignSelf: 'center', fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  You accepted drink request from {this.state.name}
                </Text>
              </View>
              :
              null}
            {this.state.offer_status == 5 || this.state.offer_status == '5' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  You offered drink to {this.state.name}
                </Text>
              </View>
              :
              null}

            {this.state.offer_status == 6 || this.state.offer_status == '6' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                {this.state.sendDrinkRequestLoading ?
                  <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                    <ActivityIndicator color={colors.white} />
                  </View>

                  :
                  <BlueButton
                    onPress={this.drinkSendRequest}
                    style={{
                      borderRadius: 5,
                      backgroundColor: colors.white,
                      // width: DEVICE_WIDTH * .35, 
                      paddingHorizontal: DEVICE_WIDTH * .02,
                      height: DEVICE_HEIGHT * .04
                    }}
                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                    REQUEST DRINK
               </BlueButton>
                }
              </View> :
              null
            }

            {this.state.offer_status == 8 || this.state.offer_status == '8' ?
              <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                  {this.state.name} has accepted your drink request
                    </Text>
              </View>
              :
              null}

          </View>

        </View>
        {/* <Dialog.Container
          visible={this.state.showReport}>
          <Dialog.Title>Report</Dialog.Title>
          <Dialog.Description>
            Do you want to report this account?
          </Dialog.Description>
          <Dialog.Input value={this.state.title} onChangeText={(title) => this.setState({ title: title })}
            label="Title" wrapperStyle={{ borderBottomColor: colors.gray, borderWidth: 1, width: DEVICE_WIDTH * .75, fontSize: DEVICE_HEIGHT * .2, color: colors.black }}></Dialog.Input>
          <Dialog.Input value={this.state.description} onChangeText={(description) => this.setState({ description: description })}
            label="Description" wrapperStyle={{ borderBottomColor: colors.gray, borderWidth: 1, marginTop: DEVICE_HEIGHT * .01, minHeight: DEVICE_HEIGHT * .1, width: DEVICE_WIDTH * .75, fontSize: DEVICE_HEIGHT * .2, color: colors.black }}></Dialog.Input>
          <Dialog.Button label="Cancel" onPress={this.handleCancel} />
          <Dialog.Button label="Report" onPress={this.handleReport} />
        </Dialog.Container> */}

        <Dialog
          onTouchOutside={() => {
            Keyboard.dismiss()
          }}
          visible={this.state.showReport}
          dialogTitle={<DialogTitle title="Report" />}
          width={DEVICE_WIDTH * .85}
          height={null}
          dialogAnimation={new ScaleAnimation({
            initialValue: 0, // optional
            useNativeDriver: true,
          })}
          footer={
            <DialogFooter>
              <DialogButton
                text="CANCEL"
                onPress={this.handleCancel}
              />
              <DialogButton
                text="REPORT"
                onPress={this.handleReport}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <View>

              <TextInputLayout
                hintColor={colors.gray}
                errorColor={colors.black}
                focusColor={colors.primaryColor}
                style={styles.mainInputLayout}
              >
                <TextInput
                  onChangeText={(title) => this.setState({ title: title })}
                  value={this.state.title}
                  style={styles.inputLayout}
                  placeholder={"Title"}
                  autoCorrect={false}
                  autoCapitalize={"none"}
                  returnKeyType={"done"}
                  placeholderTextColor={colors.gray}
                  underlineColorAndroid="transparent"
                />
              </TextInputLayout>

            </View>
            <View style={{ marginTop: DEVICE_HEIGHT * .02 }}>
              <TextInput
                onChangeText={(description) => this.setState({ description: description })}
                style={styles.inputLayoutMax}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                value={this.state.description}
                maxLength={200}
                selectionColor={colors.primaryColor}
                placeholderTextColor={colors.gray}
                // keyboardType="email-address"
                returnKeyType="done"
                multiline={true}
                placeholder="Description"
              />

            </View>
          </DialogContent>
        </Dialog>
        <GiftedChat
          showUserAvatar={false}
          messages={this.state.messages}
          onSend={this.onSend}

          // loadEarlier={this.state.loadEarlier}
          // onLoadEarlier={this.onLoadEarlier}
          // isLoadingEarlier={this.state.isLoadingEarlier}

          user={{
            _id: this.state.mytoken,
            name: this.state.myPhoneNumber,
            // avatar: this.state.myImage,
            // sent messages should have same user._id
          }}
          renderAvatar={null}
          // renderActions={this.renderCustomActions}
          renderBubble={this.renderBubble}
          renderSystemMessage={this.renderSystemMessage}
          renderCustomView={this.renderCustomView}
          renderFooter={this.renderFooter}
        />
        {/* {this.state.messages.length > 0 ?
          <GiftedChat
            showUserAvatar={false}
            messages={this.state.messages}
            onSend={this.onSend}

            // loadEarlier={this.state.loadEarlier}
            // onLoadEarlier={this.onLoadEarlier}
            // isLoadingEarlier={this.state.isLoadingEarlier}

            user={{
              _id: this.state.mytoken,
              name: this.state.myPhoneNumber,
              // avatar: this.state.myImage,
              // sent messages should have same user._id
            }}
            renderAvatar={null}
            // renderActions={this.renderCustomActions}
            renderBubble={this.renderBubble}
            renderSystemMessage={this.renderSystemMessage}
            renderCustomView={this.renderCustomView}
            renderFooter={this.renderFooter}
          /> :
          <View style={{position:'absolute',top:DEVICE_HEIGHT/2,alignSelf:'center', width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
            <ActivityIndicator  
            size='large'
            color={colors.primaryColor} />
          </View>
        } */}

      </View>

    );
  }
}

const styles = StyleSheet.create({
  mainInputLayout: {
    width: DEVICE_WIDTH * .70,

  },

  inputLayout: {
    fontFamily: fonts.normal,
    fontSize: DEVICE_HEIGHT * 0.02,
    height: DEVICE_HEIGHT * 0.06,
    color: colors.black
  },
  inputLayoutMax: {
    paddingHorizontal: DEVICE_HEIGHT * 0.01,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.gray,
    textAlignVertical: "top",
    fontFamily: fonts.normal,
    fontSize: DEVICE_HEIGHT * 0.02,
    // minHeight: DEVICE_HEIGHT * 0.1,
    height: DEVICE_HEIGHT * 0.15,
    color: colors.black
  },
  headerBackground: {
    width: DEVICE_WIDTH,
    minHeight: DEVICE_HEIGHT * 0.07,
    // height:
    //   Platform.OS == "ios" ? DEVICE_HEIGHT * 0.2 + 20 : DEVICE_HEIGHT * 0.2,
    paddingVertical: DEVICE_HEIGHT * 0.01,
    paddingHorizontal: DEVICE_WIDTH * 0.05,
    backgroundColor: colors.primaryColor,
    ...ifIphoneX(
      {
        paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
      },
      {
        // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
        paddingTop:
          Platform.OS == "ios"
            ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            : DEVICE_HEIGHT * 0.02
      }
    )
  },
  dashBoardRow: {
    flexDirection: "row",
    height: DEVICE_HEIGHT * 0.07,
    justifyContent: "space-between"
  },
  dashBoardView: {
    justifyContent: "center",
    marginTop: DEVICE_HEIGHT * 0.01
  },
  notiTouch: {
    justifyContent: "center",
    // marginTop: -(DEVICE_HEIGHT * 0.01)
  },
  backText: {
    fontFamily: fonts.normal,
    color: colors.white,
    fontSize: DEVICE_HEIGHT * 0.027
  },




  menuTouch: {
    justifyContent: "center"
  },
  menuIcon: {
    tintColor: colors.white,
    height: DEVICE_HEIGHT * 0.027,
    width: DEVICE_WIDTH * 0.027
  },

  header: {
    // marginLeft: 10,
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: DEVICE_HEIGHT * 0.027,
    alignSelf: "center",
    alignContent: "center"
  },



  footerContainer: {
    marginTop: DEVICE_HEIGHT * .02,
    marginLeft: DEVICE_WIDTH * .05,
    marginRight: DEVICE_WIDTH * .05,
    marginBottom: DEVICE_HEIGHT * .02,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});