import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';
import { fonts, colors } from '../../theme'
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import Placeholder from 'rn-placeholder';
import CardView from 'react-native-cardview'
// import pending from "../../assets/pending.png";
export default class LoadingViewUsers extends Component {
    render() {
        let screen = [];

        for (let index = 0; index < 10; index++) {
            screen.push(<View style={[styles.touchable, { marginTop: index > 0 ? 10 : 0 }]}>
                <CardView
                    style={{ backgroundColor: colors.white, height: DEVICE_HEIGHT * .12 }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={styles.touchableview}>
                        <View style={styles.imageMainView}>
                            <Placeholder.Media
                                color={colors.lightGray}
                                size={DEVICE_HEIGHT * .08}
                                hasRadius
                                animate='fade'
                                onReady={false}
                            >
                            </Placeholder.Media>
                        </View>
                        <View style={styles.numberMainView}>
                            <View style={{ flexDirection: 'column' }}>
                                <Placeholder.Line
                                    animate='fade'
                                    color={colors.lightGray}
                                    width="80%"
                                    textSize={(DEVICE_HEIGHT * 0.027) * .7}
                                    onReady={false}
                                >
                                </Placeholder.Line>
                                <Placeholder.Line
                                    style={{ marginTop: 10, }}
                                    animate='fade'
                                    color={colors.lightGray}
                                    width="60%"
                                    textSize={(DEVICE_HEIGHT * .02) * .7}
                                    onReady={false}
                                >
                                </Placeholder.Line>
                            </View>
                        </View>
                    </View>
                </CardView>
            </View>)
        }


        return (
            <View>{screen}</View>
        );
    }
}
const styles = StyleSheet.create({
    touchable: {
        height: DEVICE_HEIGHT * .12,
        // width:DEVICE_WIDTH-10,
        marginHorizontal: 10,
    },
    touchableview: {
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        height: DEVICE_HEIGHT * .12,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },
    imageMainView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        height: DEVICE_HEIGHT * .10,
        width: DEVICE_HEIGHT * .10,
        alignSelf: 'center'
    },
    numberMainView: {
        paddingHorizontal: DEVICE_WIDTH * .02,
        alignContent: 'center',
        justifyContent: 'center',
        flex: 6,
        height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        fontFamily: fonts.normal,
        color: colors.inputText,
        fontSize: (DEVICE_HEIGHT * 0.027) * .7,
    },
    dateText: {
        fontFamily: fonts.normal,
        color: colors.inputText,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },
    timeText: {
        marginLeft: 3,
        fontFamily: fonts.normal,
        color: colors.inputText,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }
});