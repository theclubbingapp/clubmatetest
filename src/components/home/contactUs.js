import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image, FlatList,Alert,
    TextInput,AsyncStorage,
    Text, TouchableOpacity,
    BackHandler,
    ScrollView, Platform,
    ImageBackground, Dimensions, KeyboardAvoidingView,ActivityIndicator

} from "react-native";
import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/register_b.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { fakeLogin } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import ModalDatePicker from "react-native-datepicker-modal";
import { CheckBox } from 'react-native-elements'
import back from "../../assets/back.png";
import FlashMessage from "react-native-flash-message";
import DateTimePicker from 'react-native-modal-datetime-picker';
let { AgeFromDateString, AgeFromDate } = require('age-calculator');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var radio_props = [
    { label: 'param1', value: 0 },
    { label: 'param2', value: 1 }
];
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const NORTH_AMERICA = ['CA', 'MX', 'US']
import DeviceInfo from 'react-native-device-info'
class ContactUs extends Component {
    constructor(props) {
        super(props);
        let userLocaleCountryCode
        DeviceInfo.isEmulator().then(isEmulator => {
            userLocaleCountryCode=isEmulator?"Simulator":DeviceInfo.getDeviceCountry();
        });
        // let userLocaleCountryCode = "+91";
        const userCountryData = getAllCountries()
            // .filter(country => NORTH_AMERICA.includes(country.cca2))
            // .filter(country => country.cca2 === userLocaleCountryCode)
            // .pop()
        let callingCode = null

        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'IN'
            callingCode = '91'
        } else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode: callingCode,
            minimumDate: new Date(),
            isDateTimePickerVisible: false,
            maleFemale: [
                { id: 1, value: "Male", isChecked: true },
                { id: 2, value: "Female", isChecked: false },
            ],
            offeringADrink: [
                { id: 1, value: "Terms & conditions", isChecked: false },
                { id: 2, value: "Privacy Policy", isChecked: false },
            ],
            loading: false,
            ph_number: '',
            password: '',
            username: '',
            dateOfBirth: 'DD-MM-YYYY',
            gender: '',
            offeringadrink: '0',
            isLoading: false,
            email: '',
            message: '',
            mytoken:''




        }

    }

    componentWillMount() {
        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })

                
            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }
    }

    getToken=()=>{

    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }


    submit = () => {

    }
    submit = () => {

        const { username, ph_number, callingCode, email, message,mytoken } = this.state;

        if (username.length == 0) {
            this.refs.refLogin.showMessage({
                message: 'Please enter full name',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        if (callingCode.length == 0) {
            this.refs.refLogin.showMessage({
                message: 'Please select country code',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (ph_number.length == 0) {
            this.refs.refLogin.showMessage({
                message: 'Please enter number',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (email == 0) {
            this.refs.refLogin.showMessage({
                message: 'Please enter email',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (!EMAIL_REGEX.test(email)) {
            this.refs.refLogin.showMessage({
                message: 'Please enter valid email',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (message.length == 0) {
            this.refs.refLogin.showMessage({
                message: 'Please enter message',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        let loginNumber = '+' + callingCode + ph_number


        let formData = new FormData()
        formData.append('authtoken', mytoken)
        formData.append('full_name', username)
        formData.append('email', email)
        formData.append('phone_number', loginNumber)
        formData.append('msg', message)
        this.setState({
            isLoading: true
        })
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.CONTACT_US, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                // this.setState({
                //     username: '',
                //     email: '',
                //     callingCode: '91',
                //     ph_number: '',
                //     message: ''

                // })
                Alert.alert(
                    'Thanks',
                    'we will get in touch with you as soon as possible',
                    [
                        {
                            text: 'OK', onPress: () => {

                            }
                        },
                    ],
                    { cancelable: true },
                );

            })
            .catch(error => {
                this.setState({
                    isLoading: false
                })

                Alert.alert(
                    'Alert',
                    'Something went wrong',
                    [
                        {
                            text: 'OK', onPress: () => {

                            }
                        },
                    ],
                    { cancelable: true },
                );
            });
    }



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);

    }

    onClickRegister = () => {
        Actions.Register();
    }

    onClickBack = () => {
        this.props.navigation.pop();
    }



    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text allowFontScaling={false}    style={styles.header}>Contact Us</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>





                </View>

                <KeyboardAwareScrollView
                    enableOnAndroid={true}
                    contentContainerStyle={{ flexGrow: 1, alignItems: 'center' }}
                    style={{}}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    scrollEnabled={true}
                >

                    <View style={{ marginTop: DEVICE_HEIGHT * .05 }}>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput allowFontScaling={false} 
                                onChangeText={(username) => this.setState({ username: username })}
                                value={this.state.username}
                                style={styles.inputLayout}
                                placeholder={"Full Name"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <View style={{ alignSelf: 'center', marginRight: DEVICE_WIDTH * .02, marginTop: DEVICE_HEIGHT * .02 }}>

                            <CountryPicker
                                // styles={}
                                // touchFlag={{height:0}}
                                // styles={styles.modalContainer}
                                flagType='flat'
                                closeable={true}
                                filterable={true}
                                showCallingCode={true}
                                onChange={value => {
                                    this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                                }}
                                cca2={this.state.cca2}
                                translation="eng"
                            />
                        </View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={[styles.mainInputLayout, { width: DEVICE_WIDTH * .75 }]}
                        >
                            <TextInput allowFontScaling={false} 
                                keyboardType='phone-pad'
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(ph_number) => this.setState({ ph_number: String.prototype.trim.call(ph_number) })}
                                style={styles.inputLayout}
                                value={this.state.ph_number}
                                placeholder={"Phone Number"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>
                    <View style={{ marginTop: DEVICE_HEIGHT * .0 }}>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}

                                onChangeText={(email) => this.setState({ email: String.prototype.trim.call(email) })}
                                value={this.state.email}
                                style={styles.inputLayout}
                                placeholder={"Email"}
                                autoCorrect={false}
                                secureTextEntry={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .0 }}>

                        <View>
                            <TextInput allowFontScaling={false} 
                                onChangeText={(message) => this.setState({ message: message})}
                                style={styles.textInput}
                                underlineColorAndroid="transparent"
                                autoCapitalize="none"
                                value={this.state.message}
                                selectionColor={colors.primaryColor}
                                placeholderTextColor={colors.textColor}
                                // keyboardType="email-address"
                                returnKeyType="default"
                                multiline={true}
                                placeholder="Message"
                            />

                        </View>

                    </View>


                    <View style={{ marginTop: DEVICE_HEIGHT * .06,alignSelf: 'center' }}>
                                        {this.state.isLoading ?

                                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                                <ActivityIndicator color={colors.primaryColor} />
                                            </View>

                                            :
                                            <BlueButton

                                                onPress={this.submit}
                                                // onPress={this.handleJoin}
                                                style={{
                                                    borderRadius:5,
                                                    backgroundColor: colors.primaryColor,
                                                    width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                }}
                                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                SUBMIT
                         </BlueButton>

                                        }


                                    </View>


                    {/* <View style={{ marginTop: DEVICE_HEIGHT * .06 }}>
                        {this.state.isLoading ?
                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                <ActivityIndicator color={colors.primaryColor} />
                            </View>

                            :
                            <BlueButton onPress={this.submit} style={{  borderRadius:5, backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * 0.027, color: colors.white }}>
                                Submit
                      </BlueButton>
                        }
                    </View> */}
                </KeyboardAwareScrollView>
                <FlashMessage ref="refLogin" />
            </View >
        );

    }
}

const styles = StyleSheet.create({
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027,
        alignSelf: "center",
        alignContent: "center"
    },
    backTouchable: {
        position: 'absolute',
        alignSelf: 'flex-start',
        left: DEVICE_WIDTH * .05,
        ...ifIphoneX({
            top: getStatusBarHeight() + DEVICE_HEIGHT * .04,
        }, {
                top: Platform.OS == 'ios' ? +DEVICE_HEIGHT * .04 : DEVICE_HEIGHT * .04,

            }),

    },
    back: {
        // marginTop: DEVICE_HEIGHT * .07,
        justifyContent: 'center',
        alignItems: 'center',
        tintColor: colors.white,

        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },


    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },
    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.04,
    }
    ,
    modelDatePickerPlaceholderStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.gray,
        paddingBottom: DEVICE_HEIGHT * .01,
    },
    dateInput: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.black,
        paddingBottom: DEVICE_HEIGHT * .01,
    }
    ,
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.textColor,
    },
    terms: {
        fontFamily: fonts.bold,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.black,
    }
    ,
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .1, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .02,
        width: DEVICE_HEIGHT * .06,
        height: DEVICE_HEIGHT * .06,
        justifyContent: 'center',
        alignItems: 'center'
    },

    textInput: {
        paddingHorizontal: DEVICE_HEIGHT * 0.01,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: colors.gray,
        textAlignVertical: "top",
        marginTop: DEVICE_HEIGHT * 0.04,
        alignSelf: "center",
        width: DEVICE_WIDTH * 0.85,
        height: DEVICE_HEIGHT * 0.15,
        // backgroundColor: colors.inputField,
        fontSize: DEVICE_HEIGHT * 0.027 * .9,
        fontFamily: fonts.normal,
        color: colors.inputText
    },

});
// export default LoginScreen;
// const mapStateToProps = ({ login }) => {
//   const { username, password, loginResponseData, isLoading,isLoggedIn } = login;


//   return {
//     username: username,
//     password: password,
//     loginResponseData: loginResponseData,
//     isLoading: isLoading,
//     isLoggedIn:isLoggedIn
//   }
// }
// export default connect(mapStateToProps, { usernameChanged, passwordChanged, showLoading, loginUser, clearLoginRecord })(LoginScreen);


const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode),
}

const mapStateToProps = state => ({
    login: state.login
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs)