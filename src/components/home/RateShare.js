import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard, Switch,
    Alert,
    FlatList,
    AsyncStorage,
    ScrollView, Slider,
    Platform, ActivityIndicator,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import BlueButton from '../common/BlueButton'
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import { Linking } from 'react-native'
class RateShare extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            mobileRecharge: [],
            page: 1,
            refreshing: false,
            sliderValue: 10,
            switchValue: false,
            isLoadingButton: false,
            error: ''
        };
    }

    change(value) {
        this.setState(() => {
            return {
                sliderValue: parseFloat(value),
            };
        });
    }
    onClickBack = () => {
        this.props.navigation.goBack();
    };
    componentWillMount() {

    }

    componentDidMount() {


    }

    openUrl = () => {
        let url = 'https://clubmate.in/';
        Linking.canOpenURL(url)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Cannot open this url');
                } else {
                    return Linking.openURL(url);
                }
            })
            .catch(err => console.log(err));

        // Linking.openURL(`tel:${'9988656565'}`)

    }

    rate = () => {
        let url = '';
        if (Platform.OS == 'ios') {
            url = 'https://play.google.com/store/apps/details?id=com.clubmate&hl=en';
        } else {
            url = 'https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8';
        }

        let phoneNumber = url;
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('cannot open this link');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));


    }



    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text  allowFontScaling={false} style={styles.header}>Rate/Share</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{
                    flex: 1, ...ifIphoneX({

                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>

                    <View style={{ marginTop: DEVICE_HEIGHT * 0.027, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View>
                            <BlueButton
                                onPress={this.rate}
                                style={{
                                    borderRadius: 5,
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * 0.027 * .9, color: colors.white }}>
                                RATE
                                      </BlueButton>


                        </View>
                        <View>
                            <BlueButton
                                onPress={this.openUrl}
                                style={{
                                    borderRadius: 5,
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * 0.027 * .9, color: colors.white }}>
                                SHARE
                                      </BlueButton>


                        </View>
                    </View>

                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RateShare);
