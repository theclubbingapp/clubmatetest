import PropTypes from 'prop-types';
import React from 'react';
import {
  Linking,
  Platform,
  StyleSheet, Dimensions,
  TouchableOpacity, Text,
  ViewPropTypes, View
} from 'react-native';
import { fonts, colors } from "../../theme";
import BlueButton from "../common/BlueButton";
import MapView from 'react-native-maps';
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
  getStatusBarHeight,
  getBottomSpace,
  ifIphoneX
} from "react-native-iphone-x-helper";
export default class CustomView extends React.Component {
  callUser = () => {
    console.log('mytoken CustomView'+this.props.myToken)
    this.props.navigation.navigate('Room', { position:this.props.position,mytoken:this.props.myToken,authtoken:this.props.authtoken,callId: this.props.callId, myName: this.props.myName, myImage: this.props.myImage })
  } 
  render() {
    
    console.log(JSON.stringify(this.props))
    if (this.props.text.includes('You have requested')||this.props.text.includes('has requested you to join call')) {
      return (
        <TouchableOpacity
          onPress={this.callUser}
          style={[styles.container, this.props.containerStyle]}>
          <View style={styles.mapView}>
            <Text  allowFontScaling={false} style={{ alignSelf: 'center', color: this.props.position == 'left' ? colors.primaryColor : colors.white, fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * .02 }}>
              Click here to Join Call
          </Text>
          </View>

        </TouchableOpacity>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: colors.gray, padding: DEVICE_HEIGHT * .01,
    // borderRadius:5,
  },
  mapView: {
    marginTop:DEVICE_HEIGHT*.02,
    height: DEVICE_HEIGHT * 0.027,
    borderRadius: 13,
    margin: 3,
  },
});

CustomView.defaultProps = {
  currentMessage: {},
  containerStyle: {},
  mapViewStyle: {},
};

CustomView.propTypes = {
  currentMessage: PropTypes.object,
  containerStyle: ViewPropTypes.style,
  mapViewStyle: ViewPropTypes.style,
};