import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity, UIManager,
    BackHandler,
    Keyboard,
    Alert,
    FlatList, Modal,
    AsyncStorage,
    ScrollView, Model, LayoutAnimation,
    Platform, ActivityIndicator,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CodeInput from 'react-native-confirmation-code-input';
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import BlueButton from '../common/BlueButton'
import { TextInputLayout } from 'rn-textinputlayout';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import moment from "moment";
import FlashMessage from "react-native-flash-message";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import applogo from "../../assets/app_logo.png";
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const NORTH_AMERICA = ['CA', 'MX', 'US']
import DeviceInfo from 'react-native-device-info';
import CountDown from 'react-native-countdown-component';
var CustomLayoutSpring = {
    duration: 400,
    create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
    },
    update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 0.7,
    },
};
class AddMembers extends Component {
    constructor(props) {
        super(props);
        let userLocaleCountryCode = DeviceInfo.isEmulator ? "Simulator" : DeviceInfo.getDeviceCountry();
        // let userLocaleCountryCode = "+91";
        const userCountryData = getAllCountries()
            .filter(country => NORTH_AMERICA.includes(country.cca2))
            .filter(country => country.cca2 === userLocaleCountryCode)
            .pop()
        let callingCode = null

        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'IN'
            callingCode = '91'
        } else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode: callingCode,
            isFromDrawer: this.props.navigation.state.params['isFromDrawer'],
            date: this.props.navigation.state.params['date'],
            time: this.props.navigation.state.params['time'],
            addedMembers: [],
            openAddMembers: false,
            showOtp: false,
            code: '',
            registration_id: this.props.navigation.state.params['registration_id'],
            group_type: this.props.navigation.state.params['group_type'],
            age: '',
            username: '',
            number: '',
            members: [],
            placeId: this.props.navigation.state.params['placeId'],
            maleOrFemale: '',
            authtoken: this.props.navigation.state.params['authtoken'],
            isLoadingMemberReg: false,
            isLoadingVerifyOtp: false,
            member_id: '',
            showAddmember: true,
            image: '',
            myphone_number: '',
            from: this.props.navigation.state.params['from'],
            tillTime: '',
            countdown: 2 * 1,
            showResend: false,
        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        this.getKey()
        console.log(this.state.group_type + "==" + this.state.registration_id)
    }

    componentWillMount() {
        if (this.state.isFromDrawer) {
            this.setState({
                addedMembers: this.props.navigation.state.params['members'],
            })
            console.log('addedMembers' + JSON.stringify(this.props.navigation.state.params['members']))
            // member_id : "20"
            // user_id : "21"
            // event_id : "83"
            // place_id : "ChIJWxfdgw3tDzkRI_GT7CncPuw"
            // member_phone_number : "+917973815030"
            // member_states : "1"
            // member_name : "Abby"
            // member_age : "99"
            // member_sex : "Male"
            // m_add_on : "2019-03-31 22:33:36"
            // m_edit : "0000-00-00 00:00:00"
            // image : "http://clubmate.dataklouds.co.in/upload_files/users/21.jpg"
        }


    }

    componentDidMount() {
        // if (this.state.date != null && this.state.time != null && this.state.date != undefined && this.state.time != undefined && this.state.date != 'undefined' && this.state.time != 'undefined') {
        //     let currentdate = new Date();
        //     let days = String(this.state.date).split('-');
        //     let day = parseInt(days[0])
        //     let month = parseInt(days[1])
        //     let year = parseInt(days[2])

        //     let time = String(this.state.time).split(':');
        //     let hour = parseInt(time[0])
        //     let min = parseInt(time[1])

        //     console.log(JSON.stringify(currentdate.getFullYear() + '==' + year))
        //     console.log(JSON.stringify(currentdate.getMonth() + '==' + month))
        //     console.log(JSON.stringify(currentdate.getDate() + '==' + day))
        //     console.log(JSON.stringify(currentdate.getHours() + '==' + hour))
        //     console.log(JSON.stringify(currentdate.getMinutes() + '==' + min))
        //     const dateToStore = year + "-" + month + "-" + day
        //     let compare = moment(dateToStore, 'YYYY-MM-DD');
        //     let now = moment();
        //     console.log(JSON.stringify(compare))
        //     console.log(JSON.stringify(now))
        //     if (now > compare) {
        //         this.setState({
        //             showAddmember: false
        //         })
        //     }

        // }
        if (this.state.from != null && this.state.from != undefined && this.state.from != 'undefined') {
            if (this.state.from == 'past') {
                this.setState({
                    showAddmember: false
                })
            } else {
                let days = String(this.state.date).split('-');
                let day = parseInt(days[0])
                let month = parseInt(days[1])
                let year = parseInt(days[2])
                const dateToStore = day + "-" + month + "-" + year
                let date = moment(dateToStore, ["DD-MM-YYYY"]).format("ddd DD-MMM-YYYY")
                this.setState({
                    tillTime: date
                })
            }
        }

    }


    // getKey = async () => {
    //     try {
    //       const myphone_number = await AsyncStorage.getItem(keys.phone);
    //       this.setState({myphone_number });
    //     } catch (error) {
    //       console.log("Error retrieving data" + error);
    //     }
    //   }

    componentDidUpdate() { }



    onClickBack = () => {
        this.props.navigation.goBack();
    };

    reset = () => {
        this.setState({
            showOtp: false,
            number: '',
            age: '',
            username: '',
            maleOrFemale: ''
        })
    }

    verifyUser = () => {
        let formData = new FormData();

        if (this.state.code.length > 6) {
            Alert.alert('Verify','Please enter six digits OTP')
            return;
        }

        formData.append('authtoken', this.state.authtoken)
        formData.append('place_id', this.state.placeId)
        formData.append('code', this.state.code)
        formData.append('register_id', this.state.registration_id)
        formData.append('member_phone_number', this.state.number)
        formData.append('member_name', this.state.username)
        formData.append('member_age', this.state.age)
        formData.append('member_sex', this.state.maleOrFemale)
        console.log('send otp' + JSON.stringify(formData))
        this.setState({
            isLoadingVerifyOtp: true
        })
        axios.post(APIURLCONSTANTS.VERIFYOTP_MEMBER, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration otp' + JSON.stringify(res))
                this.setState({
                    isLoadingVerifyOtp: false
                })

                LayoutAnimation.configureNext(CustomLayoutSpring)

                let tempObject = {
                    member_name: res['data']['data']['member_name'],
                    member_age: res['data']['data']['member_age'],
                    member_phone_number: res['data']['data']['member_phone_number'],
                    member_id: res['data']['data']['member_id'],
                    member_sex: res['data']['data']['member_sex'],

                };

                let tempArray = this.state.addedMembers
                tempArray.push(tempObject)
                this.setState({
                    addedMembers: tempArray
                })

                this.refs.refAddMember.showMessage({
                    message: this.state.username + ' added successfully',
                    type: "info",
                    position: 'bottom'
                });

                this.setState({
                    age: '',
                    username: '',
                    number: '',
                    member_id: '',
                    maleOrFemale: '',
                    showOtp: false,
                })
                this.setState({
                    openAddMembers: false,
                })

            })
            .catch(error => {
                this.setState({
                    isLoadingVerifyOtp: false
                })
                if (error.response) {
                    Alert.alert(error.response.data.msg)

                } else if (error.request) {

                    Alert.alert(error.request)
                } else {

                    Alert.alert(error.message)

                }

            });

    }

    getKey = async () => {
        try {
            // const username = await AsyncStorage.getItem(keys.username);
            const image = await AsyncStorage.getItem(keys.image);
            const myphone_number = await AsyncStorage.getItem(keys.phone);
            this.setState({ image, myphone_number });
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    submitDetails = () => {

        let formData = new FormData();

        if (this.state.username == '') {
            Alert.alert('Please enter name')
            return;
        }

        if (this.state.callingCode.length == 0) {
            Alert.alert('Please select country code')
            return;
        }

        if (this.state.number == '') {
            Alert.alert('Please enter phone number')
            return;
        }

        if (this.state.number.length == 0) {
            Alert.alert('Please enter phone number')
            return;
        }

        let loginNumber = '+' + this.state.callingCode + this.state.number

        if (this.state.myphone_number == loginNumber) {
            Alert.alert('You cannot use your own number')
            return;
        }




        if (this.state.maleOrFemale == '') {
            Alert.alert('Please select gender')
            return;
        }
        if (this.state.age == '') {
            Alert.alert('Please enter age')
            return;
        }


        formData.append('authtoken', this.state.authtoken)
        formData.append('member_phone_no', loginNumber)
        formData.append('member_name', this.state.username)
        console.log('send otp' + JSON.stringify(formData))
        this.setState({
            isLoadingMemberReg: true
        })
        axios.post(APIURLCONSTANTS.SEND_OTP_MEMBER, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration otp' + JSON.stringify(res))
                this.setState({
                    isLoadingMemberReg: false
                })
                LayoutAnimation.configureNext(CustomLayoutSpring)
                this.setState({
                    member_id: res['data']['data']['member_id']
                })
                this.setState({
                    showOtp: true
                })

            })
            .catch(error => {
                console.log('Something otp' + JSON.stringify(error))
                Alert.alert('Something went wrong!!')
                this.setState({
                    isLoadingMemberReg: false
                })
            });

    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    width: DEVICE_WIDTH,
                    height: DEVICE_HEIGHT * .02,
                }}
            />
        );
    };

    deleteItem = (item, index) => {
        let formData = new FormData();
        formData.append('member_id', item.member_id)
        formData.append('authtoken', this.state.authtoken)
        console.log('send otp' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.DELETEMEMBER, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration otp' + JSON.stringify(res))
                LayoutAnimation.configureNext(CustomLayoutSpring);


                let tempArray = this.state.addedMembers
                tempArray.splice(index, 1)
                this.setState({
                    addedMembers: tempArray
                })
                // this.setState(state => {
                //     const list = state.addedMembers.filter((item, j) => index !== j);
                //     return {
                //         addedMembers: list,
                //     };
                // });
            })
            .catch(error => {
                Alert.alert('Something went wrong!!')
                console.log('Something otp' + JSON.stringify(error))

            });

    }

    skip = () => {
        this.props.navigation.replace('UserList', { placeId: this.state.placeId, mytoken: this.state.authtoken })

    }

    onFinishCountdown = () => {
        this.setState({
            showResend: true
        })

    }

    resendOtp = () => {
        let formData = new FormData();

        if (this.state.username == '') {
            Alert.alert('Please enter name')
            return;
        }

        if (this.state.callingCode.length == 0) {
            Alert.alert('Please select country code')
            return;
        }

        if (this.state.number == '') {
            Alert.alert('Please enter phone number')
            return;
        }

        if (this.state.number.length == 0) {
            Alert.alert('Please enter phone number')
            return;
        }

        let loginNumber = '+' + this.state.callingCode + this.state.number

        if (this.state.myphone_number == loginNumber) {
            Alert.alert('You cannot use your own number')
            return;
        }




        if (this.state.maleOrFemale == '') {
            Alert.alert('Please select gender')
            return;
        }
        if (this.state.age == '') {
            Alert.alert('Please enter age')
            return;
        }


        formData.append('authtoken', this.state.authtoken)
        formData.append('member_phone_no', loginNumber)
        formData.append('member_name', this.state.username)
        console.log('send otp' + JSON.stringify(formData))
        this.setState({
            isLoadingMemberReg: true
        })
        axios.post(APIURLCONSTANTS.SEND_OTP_MEMBER, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration otp' + JSON.stringify(res))
                this.setState({
                    isLoadingMemberReg: false
                })
                LayoutAnimation.configureNext(CustomLayoutSpring)
                this.setState({
                    member_id: res['data']['data']['member_id']
                })
                this.setState({
                    showOtp: true,
                    showResend: false,
                })
                Alert.alert('Resend Otp','Otp sent successfully')

            })
            .catch(error => {
                console.log('Something otp' + JSON.stringify(error))
                // alert('Something went wrong!!')
                Alert.alert('Resend Otp','Something went wrong!!')
                this.setState({
                    isLoadingMemberReg: false
                })
            });
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{ marginTop: DEVICE_HEIGHT * 0.027, alignContent: 'center', alignSelf: 'center', borderColor: colors.gray, borderRadius: 5, borderWidth: 1, width: DEVICE_WIDTH * .95 }}>
                <CardView
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}
                    style={{ backgroundColor: colors.white, }}
                >
                    <View style={{
                        paddingHorizontal: DEVICE_WIDTH * .05,
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        paddingVertical: DEVICE_HEIGHT * .02,
                        // height: DEVICE_HEIGHT * .06
                    }}>
                        <View>
                            <Text allowFontScaling={false} style={{ color: colors.textColor, fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * 0.027, paddingHorizontal: DEVICE_WIDTH * .02 }}>
                                {item.member_name}
                            </Text>
                            <Text allowFontScaling={false} style={{ marginTop: DEVICE_HEIGHT * .01 / 2, color: colors.textColor, fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * 0.027 * .8, paddingHorizontal: DEVICE_WIDTH * .02 }}>
                                {"Age: " + item.member_age + ' Years'}
                            </Text>
                            <Text  allowFontScaling={false} style={{ marginTop: DEVICE_HEIGHT * .01 / 2, color: colors.textColor, fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * 0.027 * .8, paddingHorizontal: DEVICE_WIDTH * .02 }}>
                                {"Gender: " + item.member_sex}
                            </Text>
                        </View>
                        <BlueButton
                            onPress={() => this.deleteItem(item, index)}
                            style={{
                                backgroundColor: 'red',
                                width: DEVICE_WIDTH * .2, height: DEVICE_HEIGHT * .04
                            }}
                            textStyles={{ fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                            Delete
                </BlueButton>
                    </View>
                </CardView>
            </View>

        )

    }


    _onFulfill = (code) => {
        // alert(code)
        // if(code)
        this.setState({
            code: code
        })


    }

    addMemeber = () => {
        this.setState({
            openAddMembers: true
        })
    }

    render() {
        let maleOrFemale = [{
            value: 'Male',
        }, {
            value: 'Female',
        }];
        return (

            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        <TouchableOpacity
                            onPress={this.onClickBack}
                            style={styles.menuTouch}
                        >
                            <Image
                                source={back}
                                style={styles.menuIcon}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.dashBoardView}>
                            <Text allowFontScaling={false}  style={styles.header}>Add Memebers</Text>
                        </View>


                        <TouchableOpacity style={styles.notiTouch}>
                            {/* <Text style={{ fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                            Continue
                            </Text> */}

                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>

                    {this.state.addedMembers.length <= 5 ?
                        <View style={{ alignSelf: 'center', }}>
                            {this.state.showAddmember ?
                                <BlueButton
                                    onPress={this.addMemeber}
                                    style={{
                                        marginTop: DEVICE_HEIGHT * .05,
                                        borderRadius: 5,
                                        paddingHorizontal: DEVICE_WIDTH * .02,
                                        backgroundColor: colors.primaryColor,
                                        height: DEVICE_HEIGHT * .05
                                    }}
                                    textStyles={{ fontSize: DEVICE_HEIGHT * 0.027, color: colors.white }}>
                                    Add Members
                           </BlueButton> : null
                            }

                        </View>
                        :
                        null
                    }
                    {this.state.from == 'future' ?
                        <Text allowFontScaling={false}  style={[styles.header, { textAlign: 'center', color: colors.black, marginTop: DEVICE_HEIGHT * .02 }]}>
                            You can add members till {this.state.tillTime} only
                        </Text> :
                        null
                    }
                    {this.state.from == 'past' ?
                        <Text  allowFontScaling={false} style={[styles.header, { textAlign: 'center', color: colors.black, marginTop: DEVICE_HEIGHT * .02 }]}>
                            You can not add members now in this Entry
                        </Text> :
                        null
                    }

                    <View style={{ marginTop: DEVICE_HEIGHT * 0.027, alignContent: 'center', alignSelf: 'center', borderColor: colors.gray, borderRadius: 5, borderWidth: 1, width: DEVICE_WIDTH * .95 }}>
                        <CardView
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={5}
                            style={{ backgroundColor: colors.white, }}
                        >
                            <View style={{
                                paddingHorizontal: DEVICE_WIDTH * .05,
                                justifyContent: 'center',
                                flexDirection: 'row',
                                paddingVertical: DEVICE_HEIGHT * .02,
                                // height: DEVICE_HEIGHT * .06
                            }}>
                                <View>
                                    <View style={styles.imageMainView}>
                                        {this.state.image != '' ?
                                            <Image source={{ uri: this.state.image }}
                                                style={styles.checkImage}
                                            // resizeMode='contain'
                                            /> :
                                            <View style={[styles.checkImage, { justifyContent: 'center' }]}>
                                                <Image source={applogo}
                                                    style={styles.logoImage}
                                                    resizeMode='contain'
                                                />
                                            </View>

                                        }
                                        <Text allowFontScaling={false}  style={{ marginTop: DEVICE_HEIGHT * .02, color: colors.textColor, fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * 0.027, paddingHorizontal: DEVICE_WIDTH * .02 }}>
                                            {'You'}
                                        </Text>

                                    </View>

                                </View>

                            </View>
                        </CardView>
                    </View>

                    <FlatList
                        style={{}}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 10, paddingTop: 10, alignContent: 'center' }}
                        ref={(ref) => { this.listView = ref; }}
                        renderSeparator={this.renderSeparator}
                        data={this.state.addedMembers}
                        keyExtractor={(item, index) => `${index} - ${item}`}
                        renderItem={this.renderItem}
                        extraData={this.state}

                    />
                    {this.state.isFromDrawer ? null :
                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                            <BlueButton
                                onPress={this.skip}
                                style={{
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .05
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                CONTINUE
                    </BlueButton>
                        </View>
                    }



                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.openAddMembers}
                        onRequestClose={() => {
                            this.setState({
                                openAddMembers: false
                            })
                        }}>

                        <View style={{
                            height: DEVICE_HEIGHT,
                            backgroundColor: 'rgba(255, 255, 255, 1)',
                            ...ifIphoneX(
                                {
                                    paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                },
                                {
                                    // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                    paddingTop:
                                        Platform.OS == "ios"
                                            ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                            : DEVICE_HEIGHT * 0.02
                                }
                            )
                        }}>
                            <View allowFontScaling={false}  style={{ flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .06, backgroundColor: colors.primaryColor }}>
                                <Text  allowFontScaling={false} style={{ textAlign: 'center', flexGrow: 10, alignSelf: 'center', fontFamily: fonts.bold, color: colors.white, fontSize: DEVICE_HEIGHT * .04 }}>
                                    Add Members
                       </Text>
                                <TouchableHighlight style={{ right: 5, position: 'absolute', alignSelf: 'center', paddingRight: 10 }} onPress={() => {
                                    this.setState({
                                        openAddMembers: false,
                                        age: '',
                                        username: '',
                                        number: '',
                                        member_id: '',
                                        maleOrFemale: '',
                                        showOtp: false,

                                    })
                                }}>


                                    <Text allowFontScaling={false}  style={{
                                        alignSelf: 'center',
                                        marginLeft: DEVICE_WIDTH * .02,
                                        fontFamily: fonts.normal,
                                        fontSize: DEVICE_HEIGHT * 0.027,
                                        // height: DEVICE_HEIGHT * 0.06,
                                        color: colors.white
                                    }}>
                                        Close
                                   </Text>
                                </TouchableHighlight>
                            </View>
                            <KeyboardAwareScrollView
                                enableOnAndroid={true}
                                contentContainerStyle={{ flexGrow: 1 }}
                                style={{}}
                                resetScrollToCoords={{ x: 0, y: 0 }}

                                scrollEnabled={true}
                            >
                                <View style={{ marginTop: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                                    <TextInputLayout
                                        hintColor={colors.gray}
                                        errorColor={colors.black}
                                        focusColor={colors.primaryColor}
                                        style={styles.mainInputLayout}
                                    >
                                        <TextInput
                                         allowFontScaling={false} 
                                            onChangeText={(username) =>
                                                this.setState({ username: username })}
                                            value={this.state.username}
                                            style={styles.inputLayout}
                                            placeholder={"Name"}
                                            autoCorrect={false}
                                            autoCapitalize={"none"}
                                            returnKeyType={"done"}
                                            placeholderTextColor={colors.gray}
                                            underlineColorAndroid="transparent"
                                        />
                                    </TextInputLayout>
                                </View>
                                {/* <View style={{ marginTop: DEVICE_HEIGHT * .02, alignSelf: 'center' }}>
                                    <TextInputLayout
                                        hintColor={colors.gray}
                                        errorColor={colors.black}
                                        focusColor={colors.primaryColor}
                                        style={styles.mainInputLayout}
                                    >
                                        <TextInput
                                            keyboardType='phone-pad'
                                            onChangeText={(number) =>
                                                this.setState({ number: number })}
                                            value={this.state.number}
                                            style={styles.inputLayout}
                                            placeholder={"Number"}
                                            autoCorrect={false}
                                            autoCapitalize={"none"}
                                            returnKeyType={"done"}
                                            placeholderTextColor={colors.gray}
                                            underlineColorAndroid="transparent"
                                        />
                                    </TextInputLayout>
                                </View> */}
                                <View style={{ marginTop: DEVICE_HEIGHT * .02, flexDirection: 'row', justifyContent: 'center' }}>

                                    <View style={{ alignSelf: 'center', marginRight: DEVICE_WIDTH * .02, marginTop: DEVICE_HEIGHT * .02 }}>

                                        <CountryPicker
                                            // styles={}
                                            // touchFlag={{height:0}}
                                            // styles={styles.modalContainer}
                                            flagType='flat'
                                            closeable={true}
                                            filterable={true}
                                            showCallingCode={true}
                                            onChange={value => {
                                                this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                                            }}
                                            cca2={this.state.cca2}
                                            translation="eng"
                                        />
                                    </View>

                                    <TextInputLayout
                                        hintColor={colors.gray}
                                        errorColor={colors.black}
                                        focusColor={colors.primaryColor}
                                        style={[styles.mainInputLayout, { width: DEVICE_WIDTH * .75 }]}
                                    >
                                        <TextInput
                                         allowFontScaling={false} 
                                            //   editable={!isAuthenticating} 
                                            //   selectTextOnFocus={!isAuthenticating}
                                            keyboardType='phone-pad'
                                            onChangeText={(number) => this.setState({ number: String.prototype.trim.call(number) })}
                                            style={styles.inputLayout}
                                            value={this.state.number}
                                            placeholder={"Number"}
                                            autoCorrect={false}
                                            autoCapitalize={"none"}
                                            returnKeyType={"done"}
                                            placeholderTextColor={colors.gray}
                                            underlineColorAndroid="transparent"
                                        />
                                    </TextInputLayout>

                                </View>
                                <View style={{ marginTop: DEVICE_HEIGHT * .02, alignSelf: 'center' }}>
                                    <TextInputLayout
                                        hintColor={colors.gray}
                                        errorColor={colors.black}
                                        focusColor={colors.primaryColor}
                                        style={styles.mainInputLayout}
                                    >
                                        <TextInput
                                         allowFontScaling={false} 
                                            keyboardType='phone-pad'
                                            value={this.state.age}
                                            onChangeText={(age) =>
                                                this.setState({ age: age })}
                                            style={styles.inputLayout}
                                            placeholder={"Age"}
                                            autoCorrect={false}
                                            autoCapitalize={"none"}
                                            returnKeyType={"done"}
                                            placeholderTextColor={colors.gray}
                                            underlineColorAndroid="transparent"
                                        />
                                    </TextInputLayout>
                                </View>

                                <Dropdown
                                    containerStyle={{ width: DEVICE_WIDTH * .85, alignSelf: 'center' }}
                                    // itemTextStyle={{fontFamily:fonts.normal,color:colors.primaryColor}}
                                    // itemColor={colors.gray}
                                    selectedItemColor={colors.primaryColor}
                                    // textColor={colors.primaryColor}
                                    // value={data[0].value}

                                    baseColor={colors.primaryColor}
                                    // textColor={colors.primaryColor}
                                    fontSize={DEVICE_HEIGHT * .02}
                                    label='Select Gender'
                                    data={maleOrFemale}
                                    onChangeText={(item) => {
                                        LayoutAnimation.configureNext(CustomLayoutSpring)
                                        this.setState({ maleOrFemale: item })
                                    }}
                                />
                                {this.state.showOtp ?
                                    <View style={{ marginTop: DEVICE_HEIGHT * .1, height: DEVICE_HEIGHT * .1 }}>
                                        <CodeInput
                                            keyboardType="numeric"
                                            activeColor={colors.primaryColor}
                                            inactiveColor={colors.black}
                                            ref="codeInputRef1"
                                            secureTextEntry
                                            className={'border-circle'}
                                            codeLength={6}
                                            space={DEVICE_WIDTH * .05}
                                            size={DEVICE_HEIGHT * .06}
                                            // inputPosition='left'
                                            onFulfill={(code) => this._onFulfill(code)}
                                        />

                                    </View>
                                    : null
                                }



                                <View style={{ marginTop: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                                    {this.state.showOtp ?

                                        <View>
                                            {this.state.isLoadingVerifyOtp ?
                                                <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05 }}>
                                                    <ActivityIndicator
                                                        style={{ alignSelf: 'center' }}
                                                        size={'small'}
                                                        color={colors.primaryColor}
                                                    ></ActivityIndicator>
                                                </View>
                                                :
                                                <View>
                                                    {/* {this.state.showResend ? null :
                                                        <CountDown
                                                            until={this.state.countdown}
                                                            size={DEVICE_HEIGHT * .06}
                                                            onFinish={() => this.onFinishCountdown()}
                                                            digitStyle={{ backgroundColor: 'transparent' }}
                                                            digitTxtStyle={{ color: colors.primaryColor }}
                                                            timeToShow={['S']}
                                                            timeLabels={{ m: null, s: null }}
                                                        />

                                                    } */}



                                                    {/* <CountDown
                                                        until={this.state.countdown}
                                                        size={DEVICE_HEIGHT * .06}
                                                        onFinish={() => this.onFinishCountdown()}
                                                        digitStyle={{ backgroundColor: 'transparent' }}
                                                        digitTxtStyle={{ color: colors.primaryColor }}
                                                        timeToShow={['S']}
                                                        timeLabels={{ m: null, s: null }}
                                                    /> */}

                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH * .85 }}>
                                                        <BlueButton
                                                            onPress={this.verifyUser}
                                                            style={{
                                                                borderRadius: 5,
                                                                backgroundColor: colors.primaryColor,
                                                                width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                            }}
                                                            textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                            VERIFY
                                            </BlueButton>


                                                        {/* {!this.state.showResend ?

                                                            <BlueButton
                                                                // onPress={this.reset}
                                                                style={{
                                                                    borderRadius: 5,
                                                                    backgroundColor: colors.gray,
                                                                    width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                                }}
                                                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                                RESEND OTP
                                                          </BlueButton>
                                                            : */}
                                                            <BlueButton
                                                                onPress={this.resendOtp}
                                                                style={{
                                                                    borderRadius: 5,
                                                                    backgroundColor: colors.primaryColor,
                                                                    width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                                }}
                                                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                                RESEND OTP
                                       </BlueButton>
                                                        {/* } */}

                                                    </View>
                                                </View>

                                            }
                                        </View>


                                        :

                                        <View>
                                            {this.state.isLoadingMemberReg ?
                                                <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05 }}>
                                                    <ActivityIndicator
                                                        style={{ alignSelf: 'center' }}
                                                        size={'small'}
                                                        color={colors.primaryColor}
                                                    ></ActivityIndicator>
                                                </View>
                                                :

                                                <BlueButton
                                                    onPress={this.submitDetails}
                                                    style={{
                                                        borderRadius: 5,
                                                        backgroundColor: colors.primaryColor,
                                                        width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                    }}
                                                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                    SUBMIT
                                         </BlueButton>
                                            }

                                        </View>





                                    }





                                </View>
                            </KeyboardAwareScrollView>


                        </View>
                    </Modal>

                </View>


                <FlashMessage ref="refAddMember" />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    imageMainView: {
        // backgroundColor:'yellow',
        // flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    mainInputLayout: {
        width: DEVICE_WIDTH * .85,
    },
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },

    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        // backgroundColor: colors.white,
        // borderRadius: 5,
        // paddingHorizontal: DEVICE_WIDTH*.02,
        // paddingVertical: DEVICE_HEIGHT*.01,
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.027,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddMembers);
