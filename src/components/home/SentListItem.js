import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { fonts, colors } from '../../theme'
import CardView from 'react-native-cardview'
import BlueButton from '../common/BlueButton';
let { AgeFromDateString, AgeFromDate } = require('age-calculator');
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
export default class SentListItem extends Component {
    state = {
        otherMembers: ['+99999999', '+9999999999', '+99999999999', '+99999999999', '+99999999999'],
        isLoading: false,
    }
    componentDidMount() {
    }

    cancelRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.props.item.friend_authtoken)
        formData.append('authtoken', this.props.mytoken)
        formData.append('requestid', this.props.item.request_id)
        this.setState({
            isLoading: true
        })
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.CANCEL_FRIEND_REQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                this.props.onPress(this.props.item, this.props.index)
            })
            .catch(error => {
                this.setState({
                    isLoading: false
                })
            });
    }

    render() {
        const { navigation, item, index, onPress, onPressImage } = this.props;
        let _this = this;
        let maleFemale = '';

        if (item.gender) {
            let array = JSON.parse(item.gender);
            for (var i = 0; i < array.length; i++) {
                if (array[i].isChecked) {
                    maleFemale = array[i].value;
                }
            }
        }
        var days = String(item.dob).split('-');
        let day = parseInt(days[0])
        let month = parseInt(days[1]) - 1
        let year = parseInt(days[2])
        let ageFromString = new AgeFromDate(new Date(year, month, day)).age + ' Years';

        return (
            <View style={[styles.touchable, { marginTop: index > 0 ? 10 : 0 }]}>
                <CardView
                    style={{ backgroundColor: colors.white, }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={styles.touchableview}>
                        <TouchableOpacity onPress={() => onPressImage(item, index)} style={styles.imageMainView}>
                            <Image source={{ uri: item.image }}
                                style={styles.checkImage}
                            // resizeMode='contain'
                            />
                        </TouchableOpacity>
                        <View style={styles.numberMainView}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text allowFontScaling={false}  style={styles.numberText}>{item.full_name}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    <Text  allowFontScaling={false} style={styles.dateText}>{ageFromString}</Text>
                                    <Text  allowFontScaling={false} style={styles.timeText}>{maleFemale}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            {this.state.isLoading ?
                                <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                    <ActivityIndicator color={colors.primaryColor} />
                                </View>
                                :
                                <BlueButton
                                    onPress={this.cancelRequest}

                                    style={{
                                        borderRadius:5,
                                        backgroundColor: colors.primaryColor,
                                        width: DEVICE_WIDTH * .30, height: DEVICE_HEIGHT * .04
                                    }}
                                    textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                    CANCEL REQUEST
                           </BlueButton>
                            }

                        </View>
                    </View>
                </CardView>
            </View>

        );
    }
}
const styles = StyleSheet.create({

    est: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },

    touchable: {
        marginHorizontal: 10,
    },
    touchableview: {
        paddingVertical: DEVICE_HEIGHT * .02 * .3,
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        // height: DEVICE_HEIGHT * .12,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },

    imageMainView: {
        // flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    numberMainView: {
        paddingHorizontal: DEVICE_WIDTH * .02,
        alignContent: 'center',
        justifyContent: 'center',
        flex: 7,
        height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * 0.027) * .9,
    },
    othermembers: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    othermembersNum: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    dateText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * 0.027) * .6,
    },
    timeText: {
        marginLeft: DEVICE_WIDTH * .01,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * 0.027) * .6,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }

});
