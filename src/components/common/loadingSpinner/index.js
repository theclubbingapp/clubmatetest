import React, { Component } from 'react'
import { Dimensions, ActivityIndicator } from 'react-native'
import { View, Text } from 'native-base'

const { width, height } = Dimensions.get('window')
// import LottieView from 'lottie-react-native';
import { fonts, colors } from '../../../theme'
export default class LoadingSpinner extends Component {
  static defaultProps = {
    width,
    height,
    spinnerColor: colors.blueColor,
    textColor: colors.blueColor,
    text: ''
  };

  render() {
    return (
      <View style={{
        // backgroundColor: '#000',
        flexDirection: 'row',
        width: this.props.width, height: this.props.height, justifyContent: 'center', alignItems: 'center'
      }}
      >
        {/* <LottieView
          autoPlay={true}
          loop={true}
          style={{
            width: 24,
            height: 24,
          }}
          source={require('../../animations/ball.json')}
        /> */}
        <ActivityIndicator color={this.props.spinnerColor} />
        <View style={{ height: 10, marginLeft: 5 }} />
        <Text note style={{ color: this.props.textColor }}>{this.props.text}</Text>
      </View>
    )
  }
}