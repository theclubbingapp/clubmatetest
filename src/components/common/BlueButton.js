import React, { Component } from 'react';
import { Text , TouchableOpacity,Image,Dimensions } from 'react-native';
import ForwardArrow from "../../assets/forwardArrow.png";
import { fonts,colors } from '../../theme'
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
const BlueButton =  ({onPress,children,width,height,style,textStyles}) => {

    const {textStyle,buttonStyle} = styles;
	return(
    <TouchableOpacity onPress = {onPress} style= {[buttonStyle,style,]}>
		<Text allowFontScaling={false} style = {[textStyle,textStyles]}>
               {children}
		</Text>
    </TouchableOpacity>
	);
};

const styles = {
    textStyle: {
        fontFamily: fonts.normal,

        // fontSize: 18,
        // fontWeight: '400',
        // color:'#fff'
    },

	buttonStyle: {
        flexDirection:'row',
        justifyContent:'center',
	    alignItems:'center',
        // borderRadius: 20,
        // paddingLeft:20,
        // paddingRight:20,
		shadowOpacity:0.3,
        shadowRadius:3,
        shadowColor:'#000',
        shadowOffset:{width: 0,height:5},
        elevation: 3

	}

}

export default BlueButton;