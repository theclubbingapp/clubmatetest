import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image, FlatList,
    TextInput,
    Text, TouchableOpacity,
    BackHandler,
    ScrollView, Platform,
    ImageBackground, Dimensions, KeyboardAvoidingView

} from "react-native";
import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/register_b.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { fakeLogin } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import ModalDatePicker from "react-native-datepicker-modal";
import { CheckBox } from 'react-native-elements'
import back from "../../assets/back.png";
import FlashMessage from "react-native-flash-message";
import DateTimePicker from 'react-native-modal-datetime-picker';
let { AgeFromDateString, AgeFromDate } = require('age-calculator');
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
var radio_props = [
    { label: 'param1', value: 0 },
    { label: 'param2', value: 1 }
];
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const NORTH_AMERICA = ['CA', 'MX', 'US']
import DeviceInfo from 'react-native-device-info'
class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        // let userLocaleCountryCode = DeviceInfo.isEmulator ? "Simulator" : DeviceInfo.getDeviceCountry();
        let userLocaleCountryCode
        DeviceInfo.isEmulator().then(isEmulator => {
            userLocaleCountryCode=isEmulator?"Simulator":DeviceInfo.getDeviceCountry();
        });
        // let userLocaleCountryCode = "+91";
        const userCountryData = getAllCountries()
            // .filter(country => NORTH_AMERICA.includes(country.cca2))
            // .filter(country => country.cca2 === userLocaleCountryCode)
            // .pop()
        let callingCode = null

        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'IN'
            callingCode = '91'
        } else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode: callingCode,
            minimumDate: new Date(),
            isDateTimePickerVisible: false,
            maleFemale: [
                { id: 1, value: "Male", isChecked: true },
                { id: 2, value: "Female", isChecked: false },
            ],
            offeringADrink: [
                { id: 1, value: "Terms & conditions", isChecked: false },
                { id: 2, value: "Privacy Policy", isChecked: false },
            ],
            loading: false,
            ph_number: '',
            password: '',
            cPassword: '',
            username: '',
            dateOfBirth: 'DD-MM-YYYY',
            gender: '',
            offeringadrink: '0',
            termsCheck: false,
            privacyCheck: false,



        }

    }

    componentWillMount() {

    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        let timing = "";
        let allTime = new Date(date);
        let mon = allTime.getMonth() + 1
        let tempMonth = ''
        let tempDate = ''
        if (String(mon).length == 1) {
            tempMonth = '0' + mon
        } else {
            tempMonth = mon
        }
        if (String(allTime.getDate()).length == 1) {
            tempDate = '0' + allTime.getDate()
        } else {
            tempDate = allTime.getDate()
        }
        timing = tempDate + "-" + tempMonth + "-" + allTime.getFullYear();
        this.setState({
            dateOfBirth: timing
        })
        this._hideDateTimePicker();
    };
    handleMaleFemaleElement = (name, isChecked) => {
        let maleFemale = this.state.maleFemale
        maleFemale.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }

            // this.setState({
            //     gender: name == 'Male' ? 1 : 2
            // })

        })
        this.setState({ maleFemale: maleFemale })
    }


    handelTermCheck = () => {
        this.setState({
            termsCheck: !this.state.termsCheck
        })

    }
    handelPrivacyCheck = () => {
        this.setState({
            privacyCheck: !this.state.privacyCheck
        })
    }

    openTermAndCondition=()=>{
        Actions.TermsPrivacy()
    }
    openPrivacyPolicy=()=>{
        Actions.PrivacyTerms()
    }

    // handleDrinkElement = (name, isChecked) => {
    //     let drinkelemet = this.state.offeringADrink
    //     drinkelemet.forEach(item => {
    //         if (item.value === name) {
    //             item.isChecked = !isChecked
    //             this.setState({
    //                 offeringadrink: item.isChecked ? 0 : 1
    //             })
    //         }

    //     })

    //     this.setState({ offeringADrink: drinkelemet })
    //     console.log('offeringADrink' + JSON.stringify(this.state.offeringADrink))
    //     // if (this.state.offeringADrink[0]['isChecked']) {
    //     //     this.setState({
    //     //         termsCheck: true
    //     //     })
    //     // } else {
    //     //     this.setState({
    //     //         termsCheck: false
    //     //     })
    //     // }
    //     // if (this.state.offeringADrink[1].isChecked) {
    //     //     this.setState({
    //     //         privacyCheck: true
    //     //     })
    //     // } else {
    //     //     this.setState({
    //     //         privacyCheck: false
    //     //     })
    //     // }

    //     // console.log('terms' + this.state.termsCheck)
    //     // console.log('privacy' + this.state.privacyCheck)

    //     let checkPageOpen = (name == 'Privacy Policy' ? 1 : 0)
    //     console.log('checkPageOpen' + checkPageOpen)
    //     if (checkPageOpen == 0) {
    //         Actions.TermsPrivacy()
    //     } else {

    //         Actions.PrivacyTerms()
    //     }

    // }

    _handleDatePickedFrom = ({ date, year, day, month }) => {
        // alert(JSON.stringify(date))
        // console.log('date'+JSON.stringify(date))
        let mon = month + 1
        this.setState({
            dateOfBirth: day + "-" + mon + "-" + year + "-"
        });
    };



    goToNext = () => {
        const { callingCode, ph_number,
            password,
            username,
            dateOfBirth, maleFemale, cPassword, termsCheck, privacyCheck } = this.state;

        if (username.length == 0) {
            this.refs.refRegister.showMessage({
                message: 'Please enter username',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (ph_number.length == 0) {
            this.refs.refRegister.showMessage({
                message: 'Please enter number',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (callingCode.length == 0) {
            this.refs.refRegister.showMessage({
                message: 'Please select country code',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        if (dateOfBirth == 'DD-MM-YYYY') {
            this.refs.refRegister.showMessage({
                message: 'Please enter Date of Birth',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        if (dateOfBirth.length == 0) {
            this.refs.refRegister.showMessage({
                message: 'Please enter Date of Birth',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (password.length == 0) {
            this.refs.refRegister.showMessage({
                message: 'Please enter password',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (password.length < 6) {
            this.refs.refRegister.showMessage({
                message: 'Password must be of six character',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        if (cPassword == '') {
            this.refs.refRegister.showMessage({
                message: 'Please enter confirm password',
                type: "danger",
                position: 'bottom'
            });
            // return;
            // alert('Please enter confirm password');
            return;
        }
        if (cPassword != '') {
            if (cPassword.length < 6) {
                this.refs.refRegister.showMessage({
                    message: 'Confirm password must be of six character',
                    type: "danger",
                    position: 'bottom'
                });
                // alert('Confirm password must be of six character');
                return;
            }
        }
        if (cPassword != password) {
            this.refs.refRegister.showMessage({
                message: 'Password and Confirm password mismatch',
                type: "danger",
                position: 'bottom'
            });
            // alert('Password and Confirm password mismatch');
            return;
        }



        var days = String(this.state.dateOfBirth).split('-');
        let day = parseInt(days[0])
        let month = parseInt(days[1]) - 1
        let year = parseInt(days[2])
        let ageFromString = new AgeFromDate(new Date(year, month, day)).age;


        if (ageFromString < 18) {
            this.refs.refRegister.showMessage({
                message: 'This app is only for 18 years or older.',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        if (!this.state.termsCheck) {
            this.refs.refRegister.showMessage({
                message: 'Please select Terms & Conditions',
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        if (!this.state.privacyCheck) {
            this.refs.refRegister.showMessage({
                message: 'Please select Privacy Policy',
                type: "danger",
                position: 'bottom'
            });
            return;
        }

        let number = '+' + callingCode + ph_number
        Actions.replace("RegisterScreenSecond", { number, password, username, dateOfBirth, maleFemale })

        // Actions.RegisterScreenSecond({ number, password, username, dateOfBirth, gender,offeringadrink })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);

    }

    onClickRegister = () => {
        Actions.Register();
    }

    onClickBack = () => {
        Actions.popTo('LoginScreen');
    }

    onPressLogin = () => {
        this.props.dispatchConfirmUserLogin("ss")

    }

    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    render() {
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <ImageBackground source={Background} style={styles.img_background}>
                <TouchableOpacity onPress={this.onClickBack} style={styles.backTouchable}>
                    <Image resizeMode={'contain'}
                        style={styles.back}
                        source={back}>
                    </Image>
                </TouchableOpacity>
                <Image resizeMode={'contain'}
                    style={styles.loginLogo}
                    source={loginLogo}>
                </Image>

                <ScrollView
                    horizontal={false}
                    contentContainerStyle={{
                        justifyContent: 'center', alignContent: 'center',
                        alignItems: 'center'
                    }}>

                    <View style={{ marginTop: DEVICE_HEIGHT * .1 }}>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                onChangeText={(username) => this.setState({ username: String.prototype.trim.call(username) })}
                                style={styles.inputLayout}
                                placeholder={"Name"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>


                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <View style={{ alignSelf: 'center', marginRight: DEVICE_WIDTH * .02, marginTop: DEVICE_HEIGHT * .02 }}>

                            <CountryPicker
                                // styles={}
                                // touchFlag={{height:0}}
                                // styles={styles.modalContainer}
                                flagType='flat'
                                closeable={true}
                                filterable={true}
                                showCallingCode={true}
                                onChange={value => {
                                    this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                                }}
                                cca2={this.state.cca2}
                                translation="eng"
                            />
                        </View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={[styles.mainInputLayout, { width: DEVICE_WIDTH * .75 }]}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                keyboardType='phone-pad'
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(ph_number) => this.setState({ ph_number: String.prototype.trim.call(ph_number) })}
                                style={styles.inputLayout}
                                value={this.state.ph_number}
                                placeholder={"Phone Number"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>


                    <View style={{ marginTop: DEVICE_HEIGHT * .04 }}>
                        <TouchableOpacity style={[styles.modelDatePickerStyle, { justifyContent: 'flex-end' }]}
                            onPress={this._showDateTimePicker}>
                            <Text  allowFontScaling={false}  style={[this.state.dateOfBirth == 'DD-MM-YYYY' ? styles.modelDatePickerPlaceholderStyle : styles.dateInput]}>{String(this.state.dateOfBirth)}</Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            datePickerModeAndroid={'spinner'}
                            maximumDate={this.state.minimumDate}
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                        {/* <ModalDatePicker
                        style={styles.modelDatePickerStyle}
                        renderDate={({ year, month, day, date }) => {
                            if (!date) {
                                return (
                                    <Text style={styles.modelDatePickerPlaceholderStyle}>
                                        D.O.B - DD/MM/YYYY
                          </Text>
                                );
                            }

                            const dateStr = `${day}/${month}/${year}`;
                            return (
                                <Text style={styles.modelDatePickerStringStyle}>
                                    {dateStr}
                                </Text>
                            );
                        }}
                        onDateChanged={this._handleDatePickedFrom}
                    // startDate={this.state.startDateFrom}
                    /> */}
                        <View style={{ width: DEVICE_WIDTH * .85, height: 1, backgroundColor: colors.gray }}>

                        </View>
                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .0 }}>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}

                                onChangeText={(password) => this.setState({ password: String.prototype.trim.call(password) })}
                                style={styles.inputLayout}
                                placeholder={"Password"}
                                autoCorrect={false}
                                secureTextEntry={true}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .0 }}>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}

                                onChangeText={(cPassword) => this.setState({ cPassword: String.prototype.trim.call(cPassword) })}
                                style={styles.inputLayout}
                                placeholder={"Confirm Password"}
                                autoCorrect={false}
                                secureTextEntry={true}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>

                    <View style={{ paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .027, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Gender
                                </Text>
                        <FlatList
                            style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                            contentContainerStyle={{}}
                            data={this.state.maleFemale}
                            renderItem={({ item }) => (
                                <CheckBox
                                    // handleCheckChieldElement={this.handleCheckChieldElement}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    title={item.value}
                                    containerStyle={{ width: (DEVICE_WIDTH * .85) / 3, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => this.handleMaleFemaleElement(item.value, item.isChecked)}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}

                                // {...typesOfDrink} 
                                />
                            )}
                            extraData={this.state}
                            horizontal={true}
                            //Setting the number of column
                            // numColumns={3}
                            keyExtractor={(item, index) => index.toString()}

                        />
                    </View>

                    <View style={{ flexDirection: 'row', paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .027, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <CheckBox
                                // title={''}
                                containerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, }}
                                checked={this.state.termsCheck}
                                checkedColor={colors.primaryColor}
                                onPress={() => this.handelTermCheck()}
                                size={DEVICE_HEIGHT * 0.02}

                            />
                            <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.openTermAndCondition}>
                            <Text  allowFontScaling={false} style={[styles.terms, { alignSelf: 'center' }]}>
                                {'Terms & Conditions'}
                            </Text>
                            </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <CheckBox
                            // title={''}
                            containerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, }}
                            checked={this.state.privacyCheck}
                            checkedColor={colors.primaryColor}
                            onPress={() => this.handelPrivacyCheck()}
                            size={DEVICE_HEIGHT * 0.02}

                        />
                        <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.openPrivacyPolicy}>
                            <Text  allowFontScaling={false} style={[styles.terms, { alignSelf: 'center' }]}>
                                {'Privacy Policy'}
                            </Text>
                        </TouchableOpacity>
                    </View>

                    {/* <FlatList
                            style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                            contentContainerStyle={{}}
                            data={this.state.offeringADrink}
                            renderItem={({ item }) => (
                                <CheckBox
                                    title={item.value}
                                    containerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => this.handleDrinkElement(item.value, item.isChecked)}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.terms]}
                                />
                            )}
                            extraData={this.state}
                            horizontal={true}
                            keyExtractor={(item, index) => index}

                        /> */}
                    </View>


                <View style={{ marginTop: DEVICE_HEIGHT * .06 }}>
                    <BlueButton onPress={this.goToNext} style={{ backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * .027, color: colors.white }}>
                        NEXT
                </BlueButton>
                </View>
                <View style={{ marginTop: DEVICE_HEIGHT * .027, marginBottom: DEVICE_HEIGHT * .027, paddingRight: DEVICE_WIDTH * .05, paddingLeft: DEVICE_WIDTH * .05, width: DEVICE_WIDTH, flexDirection: 'row', justifyContent: 'center' }}>
                    <View style={{ alignSelf: 'center' }}>
                        <BlueButton onPress={this.onClickBack} textStyles={{ fontSize: DEVICE_HEIGHT * .027, color: colors.primaryColor }}>
                            <Text style={{ color: colors.black }}>
                                Already have an account?
                            <Text style={{ color: colors.primaryColor }}>Login</Text>
                            </Text>

                        </BlueButton>
                    </View>

                </View>
                </ScrollView>
            <FlashMessage ref="refRegister" />

            </ImageBackground >
        );

    }
}

const styles = StyleSheet.create({
    backTouchable: {
        position: 'absolute',
        alignSelf: 'flex-start',
        left: DEVICE_WIDTH * .05,
        ...ifIphoneX({
            top: getStatusBarHeight() + DEVICE_HEIGHT * .04,
        }, {
                top: Platform.OS == 'ios' ? +DEVICE_HEIGHT * .04 : DEVICE_HEIGHT * .04,

            }),

    },
    back: {
        // marginTop: DEVICE_HEIGHT * .07,
        justifyContent: 'center',
        alignItems: 'center',
        tintColor: colors.white,

        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.027,
        width: DEVICE_WIDTH * 0.027
    },


    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },
    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.04,
    }
    ,
    modelDatePickerPlaceholderStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.gray,
        paddingBottom: DEVICE_HEIGHT * .01,
    },
    dateInput: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.black,
        paddingBottom: DEVICE_HEIGHT * .01,
    }
    ,
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        color: colors.textColor,
    },
    terms: {
        fontFamily: fonts.bold,
        fontSize: DEVICE_HEIGHT * 0.027 * .9,
        color: colors.black,
    }
    ,
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.027,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .1, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .02,
        width: DEVICE_HEIGHT * .06,
        height: DEVICE_HEIGHT * .06,
        justifyContent: 'center',
        alignItems: 'center'
    }



});
// export default LoginScreen;
// const mapStateToProps = ({ login }) => {
//   const { username, password, loginResponseData, isLoading,isLoggedIn } = login;


//   return {
//     username: username,
//     password: password,
//     loginResponseData: loginResponseData,
//     isLoading: isLoading,
//     isLoggedIn:isLoggedIn
//   }
// }
// export default connect(mapStateToProps, { usernameChanged, passwordChanged, showLoading, loginUser, clearLoginRecord })(LoginScreen);


const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode),
}

const mapStateToProps = state => ({
    login: state.login
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)