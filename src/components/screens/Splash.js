import React, { Component } from 'react';

import { connect } from 'react-redux';

import {
	AppRegistry,
	StyleSheet,
	Dimensions,
	View,
	Image,
	Platform,
	AsyncStorage,
	Text
} from 'react-native';

//var isLogin = false;
import { Actions } from 'react-native-router-flux';

import splashImage from "../../assets/app_logo.png";
import { colors } from '../../theme';
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
// const isLogin = false;
export default class Splash extends Component {

	componentWillMount() {
		setTimeout(() => {
			Actions.reset('IntroScreen');
	// 		AsyncStorage.getItem("userData").then((token) => {
	// 			if (token) {
	// 				usertype = JSON.parse(token).type;
	// 				if (token.length > 0) {
	// 				} else {
	// 					// Actions.IntroScreen();
	// 					Actions.reset('IntroScreen');
	// 				}
	// 			}
	// 			else{
	// 				// Actions.IntroScreen();
	// 				Actions.reset('IntroScreen');
	// 			}
	// 		}
	// )
		}, 2000);


	}

	render() {
		return (
			<View style={{ flex: 1,alignItems:'center', justifyContent: 'center' ,backgroundColor:colors.primaryColor}}>
							<Image 
							resizeMode={'contain'}
							style={{ width: DEVICE_HEIGHT*.4, height: DEVICE_HEIGHT*.4, justifyContent: 'center', alignItems: 'center' }}
					source={splashImage}>
				</Image>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	background: {
		flex: 1,
		width: null,
		height: null,
		alignItems: 'center',
		justifyContent: 'center',
	},

	logoContainer: {
		position: 'absolute',
		alignItems: 'center',
	},
});