const keys = {
    userId: "userId",
    isLoggedIn: "isLoggedIn",
    firstName: 'firstName',
    username: 'username',
    lastName: 'lastName',
    image: 'image',
    fcmToken: 'fcmToken',
    gotoFriendScreen: 'gotoFriendScreen',
    phone: 'phone',
    email: 'email',
    token: 'token'
};
export default keys;
